const BgConstants = {
    HOST_NAME: "localhost",
    START_URL: "http://localhost:80/",
}

export default BgConstants;