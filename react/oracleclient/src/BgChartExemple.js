import './BgChartExemple.css';
import { Vega } from 'react-vega';
const React = require('react');


const vegaDescriptorConstante1 = {
    width: 400,
    height: 100,
    mark: 'point', // 'circle' , //'line',
    encoding: {
        x: { field: 'time', type: 'temporal', scale: { zero: false, }, },
        y: { field: 'y', type: 'quantitative', tittle: "yyyyy", scale: { zero: false, }, },
        color: { field: "label", type: "nominal" },
    },
    data: {
        name: "tableBg",
        url: "",
        format: {
            zero: false,
        }
    }
}



class BgChartExemple extends React.Component {

    constructor(properties) {

        super(properties)
        this.state = {
            specVega: vegaDescriptorConstante1,
            curves: [],
            urls: [],
            
        };
        this.addNewCurve = this.addNewCurve.bind(this);
        this.selectCurve = this.selectCurve.bind(this);
        this.getVisible = this.getVisible.bind(this);
    }

    componentDidMount() { 
        this._ismounted = true;
      }


    addNewCurve(cc){
        let curves2  = this.state.curves;
        curves2.push(cc);
        this.setState(
            {
                curves: curves2,
            }
        );
        this.selectCurve(curves2.length-1);
    }

    selectCurve(i){
        this.setState(
            {
                iSelected: i,
            }
        );
    }

    getVisible(i) {
        if (i === this.state.iSelected){
            return 'bgVisible'
        }else{
            return 'bgHidden2'
        }
    }

    urls = [];
    render() {
        this.state.specVega.data.url = this.props.url;
        const isAlreadyCollected = this.state.urls.some(url => (url === this.props.url));
        if (isAlreadyCollected ) {
            console.log("Bingo isAlreadyCollected !!!!!!!!!!!!!!!!");
            const i = this.state.urls.findIndex ((url) => (url === this.props.url));
            console.log("Bingo isAlreadyCollected !!!!!!!!!!!!!!!! i : "+i);
            if (this._ismounted){
                if (this.state.iSelected != i){
                    this.selectCurve(i);
                }
               
            }
            
        }else {
            console.log("Bingo new curves ");
            this.state.specVega.data.url = this.props.url;
            const specvega =  this.state.specVega ;
            this.state.urls.push(this.props.url);
            const cc = <div className="bgVisible"> {this.state.specVega.data.url }<Vega id={"Vega" + this.props.id} spec={specvega} /></div>;
            this.addNewCurve(cc);
        }
        console.log("isAlreadyCollected " + (isAlreadyCollected === true) + "   " + this.props.url, this.state.urls)
        console.log("curves " + this.state.curves.length, this.state.curves)
        return (
            <div className="BgChartExemple">
                {this.props.titleBg}
                <div className="blue"> 
                    {this.state.curves.map((x,i)=>(<div className={this.getVisible(i)}> {i+1}  {x} </div>))};
                </div>     
            </div>
        );

    }
}

export default BgChartExemple;

export { vegaDescriptorConstante1 }