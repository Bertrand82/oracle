import BgChartExemple from "./BgChartExemple"
import {vegaDescriptorConstante1} from "./BgChartExemple"
import BgConstants from "./BgConstants"
import Select from 'react-select'
import { CompositeMarkNormalizer } from "vega-lite/build/src/compositemark/base";
import './BgOracleViewer.css';
const React = require('react');
const optionsDeviseCst = [
    { value: 'BTC', label: 'BTC', selected: true },
    { value: 'ADA', label: 'ADA', selected: true },
    { value: 'XRP', label: 'XRP', selected: true }
]

const sources = ["kraken", "bitfinex", "wikipedia", "twitter"];
const sourcesCst = [
    {value: "kraken",label: 'kraken', selected: true}, 
    {value:"bitfinex",label: 'bitfinex', selected: true}, 
    {value:"wikipedia",label: 'wikipedia', selected: true},
    {value:"twitter",label: 'twitter', selected: true},
    ];
const intervalles = ["D", "H"];
const urlBase = "http://localhost/multisources/D?";
const url0 = urlBase + "id=bg1&sources=kraken&devises=BTC";

class BgOracleViewer extends React.Component {
    constructor(properties) {
        super(properties);
        this.state = {
            too: 'tttotototototootto',
            devises2: optionsDeviseCst,
            sources2: sourcesCst,
            url2: url0,
            urlsTwitterDay: [],
            urlsTwitterHour: [],
            urlsKrakenDay: [],
            urlsKrakenHour: [],
            optionsDevises: optionsDeviseCst,
            deviseSelected: 'NO DEVISE',
            displayH: false,
            displayD: false,
            vegaDescriptor: vegaDescriptorConstante1,
        }

        this.fetchDevises = this.fetchDevises.bind(this);
        this.processUrls = this.processUrls.bind(this);
        this.selectOption = this.selectOption.bind(this);
        this.updateDevise = this.updateDevise.bind(this);
        this.updateSource = this.updateSource.bind(this);
        this.updateUrl = this.updateUrl.bind(this);
        //this.fetchDevises();
    }
    /**
     * La méthode componentDidMount() est exécutée après que la sortie du composant a été injectée dans le DOM. 
     */
    async componentDidMount() {
        console.log("componentDidMount start")
        await this.fetchDevises();
        this.processUrls();

    }




    async fetchDevises() {
        const url = BgConstants.START_URL + "devises"
        console.log("fetchDevises url :  ", url);
        return fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("reponse json", responseJson);
                this.setState({
                    devises: responseJson,
                })
                for (const [index, value] of responseJson.entries()) {
                    console.log("  devise " + index + " :", value);

                }

            })
            .catch((error) => {
                console.error(error);
            });
    }
    processUrls() {
        console.log("processUrls ", this.state.devises);
        const urlsTwitterDay = []
        const urlsTwitterHour = []
        const urlsKrakenDay = []
        const urlsKrakenHour = []
        for (const [index, value] of this.state.devises.entries()) {
            let urlTwitterDay = "http://localhost/twittercount/D/" + value + "?total=10000"
            let urlTwitterHour = "http://localhost/twittercount/H/" + value + "?total=10000"
            let urlKrakenDay = "http://localhost/kraken/D/" + value + "?total=10000"
            let urlKrakenHour = "http://localhost/kraken/H/" + value + "?total=10000"
            console.log("urlDay  " + index, urlTwitterDay)
            urlsTwitterDay.push(urlTwitterDay)
            urlsTwitterHour.push(urlTwitterHour)
            urlsKrakenDay.push(urlKrakenDay)
            urlsKrakenHour.push(urlKrakenHour)
        }
        this.setState({
            urlsTwitterDay: urlsTwitterDay,
            urlsTwitterHour: urlsTwitterHour,
            urlsKrakenDay: urlsKrakenDay,
            urlsKrakenHour: urlsKrakenHour,
        })
        this.processOptions()
    }
    processOptions() {
        console.log("processOption ", this.state.devises);
        const optionsDevisesV = []
        for (const [index, value] of this.state.devises.entries()) {
            const r = { value: value, label: value }
            optionsDevisesV.push(r)
        }
        this.setState({
            optionsDevises: optionsDevisesV,
        })
    }

    selectOption(e) {
        console.log("selectOption", e, e.value);
        console.log("selectOption2", e.value);
        this.setState({
            deviseSelected: e.value,
            displayIntervalle: 'xxx',
        });
    }

    updateDevise(devises22) {
        this.setState({
            devises2: devises22,
        })
    }
    updateSource(source22) {
        this.setState({
            sources2: source22,
        })
    }
    updateUrl() {
       
        let sourcesRequest = "";    
        for (let source of this.state.sources2) {
            console.log("updateUrl source:"+source.label+"  selected", source.selected);
            if (source.selected){
                sourcesRequest +="&sources="+source.value;
            }
        }
        let devisesRequest= '';
        for (let devise of this.state.devises2) {
            console.log("updateUrl devise:"+devise.label+"  selected", devise.selected);
            if (devise.selected) {
               devisesRequest+="&devises="+devise.value;
            }
        }
        
        const url00 = urlBase + "id=bg1"+sourcesRequest+devisesRequest;
        let dVega = vegaDescriptorConstante1;
        dVega.data.url = this.props.url;
        console.log("updateUrl url0",url00)
        this.setState({
            url2: url00,
            vegaDescriptor: dVega,
        })
    }

    render() {

        const sourcesArray = [];
       
        for (let source of  this.state.sources2) {
            const checkBox = <label className="bg"><input id={"source"+source.value} type="checkbox" onClick={(e) => { source.selected=!source.selected; this.updateSource(this.state.sources2);}} value={this.state.displayH}  checked={source.selected}/>{source.value}</label>
            sourcesArray.push(checkBox);
        }
        const devisesArray = [];
        for (let devise of this.state.devises2) {
            console.log("Viewver devise:", devise);
            const checkBox = <label  className="bg"><input id={"devise"+devise.value} type="checkbox" onClick={(e) => { devise.selected = !devise.selected; this.updateDevise(this.state.devises2); }} checked={devise.selected} />{devise.label}</label>
            devisesArray.push(checkBox);
        }
        



        return (
            <div>
                <div id="trace"></div>

                <div className="bgButtons">
                    <button className="bg" onClick={this.fetchDevises} >fetch devises</button>
                    <button className="bg" onClick={this.processUrls}>Process</button>
                    <label className="bg"><input type="checkbox" onClick={() => { this.setState({ displayH: !this.state.displayH, }) }} value={this.state.displayH} />Hourly</label>
                    <label className="bg"><input type="checkbox" onClick={() => { this.setState({ displayD: !this.state.displayD, }) }} value={this.state.displayD} />Daily</label>
                    <Select
                        className="bg"
                        onChange={this.selectOption}
                        options={this.state.optionsDevises}
                        selected={this.state.deviseSelected}
                    />
                </div>
                <div>
                    <div> {sourcesArray} </div>
                    <div> {devisesArray} </div>
                    <input className="bg" onClick={this.updateUrl} type="submit" value="Fire" />

                </div>
                <BgChartExemple key={'key22_'} id='aaaaa' vegaDescriptor={this.state.vegaDescriptor} url={this.state.url2} label={this.state.url2 + " "} titleBg="bg33" />

            </div>
        )
    }


}


export default BgOracleViewer