import Select from 'react-select'
import './BgBoard.css';
const React = require('react');

class BgBoard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            source: 'all',
            intervalle: 'all',
        };
        this.command = this.command.bind(this);
        this.handleChangeSources = this.handleChangeSources.bind(this);
        this.handleChangeIntervalle = this.handleChangeIntervalle.bind(this);
        this.handleSubmitFetchTest = this.handleSubmitFetchTest.bind(this);
    }



    command(url) {
        console.log("command start", url)
        fetch(url)
            .then(response => response.json())
            .then(data => console.log("data", data));
    }

    handleChangeSources(event) {
        console.log("-----------------event ", event);
        this.setState({ source: event.value });
    }
    handleChangeIntervalle(event) { this.setState({ intervalle: event.value }); }
    handleSubmitFetchTest(event) {
        let url = "http://localhost/board/start/" + this.state.source + "/" + this.state.intervalle
        alert('Source Selected url: ' + url);
        fetch(url).then(response => { console.log("retour fetch", response); return response.text(); })
            .then(text => { console.log("body2:text ", text); return text })
            .then( text => {document.getElementById("bgLog").innerHTML=text; return text})

    }



    render() {
        const sources = [
            { value: 'bitfinex', label: 'bitfinex' },
            { value: 'kraken', label: 'kraken' },
            { value: 'twitter', label: 'twitter' },
            { value: 'wikipedia', label: 'wikipedia' },
            { value: 'all', label: 'all' },
        ];
        const intervalles = [
            { value: 'D', label: 'Daily' },
            { value: 'H', label: 'Hourly' },
            { value: 'all', label: 'all' },
        ]
        return (
            <div className="bgBoard">
                <h2>Board</h2>
                <div>

                    <label className="bgBoard">Source :<Select className="bgBoard" options={sources} onChange={this.handleChangeSources} title="Source" /></label>
                    <label className="bgBoard">Intervalle :<Select className="bgBoard" options={intervalles} onChange={this.handleChangeIntervalle} /></label>
                    <input className="bgBoard" onClick={this.handleSubmitFetchTest}  type="submit" value="Fire watch dog" />

                </div>
                <div>
                    <div className="bg">
                        <a href="http://localhost/twitter/D/BTC?total=10000">Twitter BTC </a>
                    </div>
                    <div className="bg">
                        <a href="http://localhost/kraken/H/BTC">Kraken H BTC</a>
                    </div>
                </div>
                <div id="bgLog" className="bgBoard">

                </div>
            </div>
        )
    }
}
export default BgBoard;