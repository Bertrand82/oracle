package bg.curve;

public class PointBg {

	public double x;
	public long time;
	public PointBg(long i, double x2) {
		this.time = i;
		this.x = x2;
	}
	public PointBg(double x2,long i) {
		this.time = i;
		this.x = x2;
	}
	@Override
	public String toString() {
		return "PointBg [x=" + x + ", time=" + time + "]";
	}
	
}





