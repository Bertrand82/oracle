package bg.curve.swing;

import java.awt.BorderLayout;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GeneratorGui {

	
	private JFrame frame = new JFrame("Bruit");
	private CanvasCurve canvas = new CanvasCurve();
	
	public GeneratorGui() {
		JPanel panelGlobal = new JPanel(new BorderLayout());
		panelGlobal.add(canvas);
		frame.add(panelGlobal);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
	}
	
	public void traceCurveAndPrediction(CourbeBg ... curves){
		this.canvas.tracerCourbeAndPrediction(Arrays.asList(curves));
	}

	public void traceCurve(CourbeBg ... curves) {
		
		this.canvas.tracerCourbe(Arrays.asList(curves));
	}
}



