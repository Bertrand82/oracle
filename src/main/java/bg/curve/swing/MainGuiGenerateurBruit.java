package bg.curve.swing;

import java.awt.Color;

public class MainGuiGenerateurBruit {

	public static void main(String[] args) {
		GeneratorGui generatorGui = new GeneratorGui();
		CourbeBg curve1 = GeneratoFactory.createCurveSinusoidale();
		CourbeBg curve2 = GeneratoFactory.createCurveAleatoire(Color.blue);
		CourbeBg curve3 = GeneratoFactory.createCurveAleatoire(Color.red);
		CourbeBg curve4 = GeneratoFactory.createCurveAleatoire(Color.black);
		generatorGui.traceCurve(curve1, curve2,curve3,curve4);
	}

}



