package bg.curve.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class CanvasCurve extends JPanel{

	boolean tracerPrediction=false;
	int width = 200;
	int height = 100;
	List<CourbeBg> courbes = new ArrayList<CourbeBg>();
	public CanvasCurve() {
		super();
		Dimension d = new Dimension(width+10, height+10);
		this.setSize(d);
		this.setPreferredSize(d);		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		try {
			g.setColor(Color.black);
			g.drawLine(10, 10, 10, height-10);
			g.drawLine(10, height-10, width-10, height-10);
			for(CourbeBg courve : courbes){
				g.setColor(courve.getColor());
				for(Point p : courve.getPoints2D(width-20,height-10,10, height-10)){
					g.drawOval(p.x, p.y, 1, 1);
				}
				if (tracerPrediction){
					g.setColor(courve.getColorPrediction());
					for(Point p : courve.getPoints2DPrediction(width-20,height-10,10, height-10)){
						g.drawOval(p.x, p.y, 1, 1);
					}
				}
			
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void tracerCourbe(List<CourbeBg> list){
		tracerPrediction=false;
		this.courbes.addAll(list);
		repaint();
	
	}

	public void tracerCourbeAndPrediction(List<CourbeBg> asList) {
		tracerPrediction=true;
		this.courbes.addAll(asList);
		repaint();
	
	}
	
	
}



