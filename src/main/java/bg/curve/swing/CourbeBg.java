package bg.curve.swing;


import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import bg.curve.PointBg;
import bg.curve.analyseur.math.FrequenciesAnalyse;

public class CourbeBg extends ArrayList<PointBg>{
	
	private static final long serialVersionUID = 1L;
	double maxX = -1;;
	double minX = -1;
	long maxT_0 = -1;
	long maxT = -1;
	long minT = -1;
	long deltaT_0;
	Color color = Color.black;
	Color colorPrediction = Color.red;
	FrequenciesAnalyse frequenceAnlayse;
	List<PointBg> listPointsPrediction = new ArrayList<PointBg>();
	public CourbeBg() {
		super();
	}
	public CourbeBg(Color color) {
		super();
		this.color = color;
	}
	
	public void init(){
		initMinMax();
		
		this.frequenceAnlayse= new FrequenciesAnalyse(this);
		int nbPoints = this.size()/10;
		for(int i=1; i<nbPoints;i++){
			long t = maxT_0+i*deltaT_0/this.size();
			double x = this.frequenceAnlayse.getX(t);
			PointBg point = new PointBg(x, t);
			this.listPointsPrediction.add(point);
		}
	}
	
	private void initMinMax() {
		for(PointBg p : this){
			
			if (minT ==-1){
				minT = p.time;
				maxX =p.x;
				minX =p.x;
				maxT_0 = p.time;
			}			
			maxT_0 = Math.max(p.time, maxT_0);
			minT = Math.min(p.time, minT);
			maxX = Math.max(p.x, maxX);
			minX = Math.min(p.x, minX);
			maxT = maxT_0 + (int) (0.2 * (maxT -minT));
			deltaT_0 =maxT_0-minT;
		}
	}

	public List<Point> getPoints2D(int lTime,int lX, int t0 , int x0) {
		init();
		return getPoints2D(lTime,lX,t0,x0,this);
	}
	public List<Point> getPoints2DPrediction(int lTime,int lX, int t0 , int x0) {
		return  getPoints2D(lTime,lX,t0,x0,this.listPointsPrediction);
	}
	
	private  List<Point> getPoints2D(int lTime,int lX, int t0 , int x0,List<PointBg> list) {
		List<Point> l = new ArrayList<Point>();
		for(PointBg pBg : list){
			int t = (int) ( lTime*(pBg.time-minT)/(maxT-minT));
			int x = (int) ( lX * (pBg.x-minX)/(maxX - minX));
			int tt  = t0 + t;
			int xx = x0 -x;
			Point pp = new Point(tt, xx);
			l.add(pp);
		}
		return l;
	}
	
	


	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	public Color getColorPrediction() {
		return colorPrediction;
	}
	public void setColorPrediction(Color colorPrediction) {
		this.colorPrediction = colorPrediction;
	}
	
	
	
	
}

