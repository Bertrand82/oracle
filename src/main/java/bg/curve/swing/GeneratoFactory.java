package bg.curve.swing;

import java.awt.Color;
import java.util.Random;

import bg.curve.PointBg;

public class GeneratoFactory {

	public static Random rand = new Random(); 
	
	public static CourbeBg createCurveSinusoidale() {
		return createCurveSinusoidale(Color.BLACK,20,200, Math.PI/4, 12.0);
	}
	public static CourbeBg createCurveSinusoidale(Color c, double amplitude,double periode, double phase,double moyenne) {
				CourbeBg curve = new CourbeBg();
		for(int i=0;i<1000;i++){
			double x = amplitude* Math.cos((2*Math.PI*i/200)+phase)+moyenne;
			PointBg p = new PointBg(i, x);
			curve.add(p);
		}
		return curve;
	}
	
	
	public static CourbeBg createCurveAleatoire() {
		return createCurveAleatoire(Color.blue);
	}
	public static CourbeBg createCurveAleatoire(Color color) {
		
		CourbeBg curve = new CourbeBg(color);
		double xZ_1 =0;
		for(int i=0;i<1000;i++){
			double r = rand.nextDouble()-0.5;
			double x = xZ_1+r;
			PointBg p = new PointBg(i, x);
			curve.add(p);
			xZ_1=x;
		}
		return curve;
	}


}



