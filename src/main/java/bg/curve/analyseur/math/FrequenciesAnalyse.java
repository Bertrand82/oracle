package bg.curve.analyseur.math;

import java.util.ArrayList;
import java.util.List;

import bg.curve.PointBg;

import static java.lang.Math.*;

/**
 * 
 * @author bg Time Series Analysis Extrait les frequences et parametres
 *         significatis d'une s�rie de mesures TODO: Evaluer les prochains
 *         points bas et points hauts (A partir du dernier point haut par
 *         exemple, estimer la date du prochain point haut. La valeur sera la
 *         meme que le precedent. )
 */
public class FrequenciesAnalyse {

	private long nbDePoints = 0;
	public long period = 0;
	private long periodEcartType = 0;
	private double amplitudeMoyenne = 0;
	private double amplitudeEcartType = 0;
	public FrequenciesAnalyse frequenceAnalyseNext;
	private PointBg lastPointHaut;
	private PointBg lastPointBas;
	private double phaseMoyenne = 0;
	private double amplitude = 0;
	private double lastPhase = 0;
	private double moyenne = 0.0;

	public FrequenciesAnalyse(List<PointBg> points) {
		process(points);
	}

	private void process(List<PointBg> points) {
		this.processMoyenne(points);
		this.processFrequence(points);

	}

	private void processMoyenne(List<PointBg> points) {
		double moyenneSum= 0;
		for( PointBg p:points) {
			moyenneSum+=p.x;
		}
		if (points.size()>0) {
			moyenne=moyenneSum/points.size();
		}
	}

	private void processFrequence(List<PointBg> points) {
		this.nbDePoints = points.size();
		List<PointBg> pointsMax = getPoinsMaxFromList(points);
		List<PointBg> pointsMin = getPointMinFromList(points);
		List<Long> listDelta = new ArrayList<Long>();
		List<Double> listAmplitude = new ArrayList<Double>();
		List<Double> listPhases = new ArrayList<Double>();
		long deltaSum = 0;
		double amplitudeSum = 0;
		
		double moyenneSum =0;
		for (int i = 1; i < pointsMax.size(); i++) {
			PointBg p0 = pointsMax.get(i - 1);
			PointBg p1 = pointsMax.get(i);
			long delta = p1.time - p0.time;
			PointBg pMin = pointsMin.get(i - 1);
			double amplitude = Math.abs(p1.x - pMin.x) / 2;

			listAmplitude.add(amplitude);
			listDelta.add(delta);
			deltaSum += delta;
			amplitudeSum += amplitude;
			moyenneSum += p0.x;
		}
		
		long ecartSum = 0;
		if (listDelta.isEmpty()) {
			return;
		}
		this.period = deltaSum / listDelta.size();
		this.amplitudeMoyenne = (amplitudeSum / listDelta.size());
		for (Long delta : listDelta) {
			ecartSum += Math.abs(delta - period);

		}
		this.periodEcartType = ecartSum / listDelta.size();
		double ecartAmplitudeSum = 0;
		for (Double a : listAmplitude) {
			ecartAmplitudeSum += (a - this.amplitudeMoyenne);
		}
		this.amplitudeEcartType = ecartAmplitudeSum / listAmplitude.size();
		frequenceAnalyseNext = new FrequenciesAnalyse(pointsMax);
		this.lastPointHaut = pointsMax.get(pointsMax.size() - 1);
		this.lastPointBas = pointsMin.get(pointsMin.size() - 1);
		// calcul de la phase
		double phaseSum = 0;
		double phase0 = 0;
		double phase1 = 0;
		for (int i = 1; i < pointsMax.size(); i++) {
			this.lastPhase = phase1;// Ne pas prendre le dernier point pour
									// l'eval de la phase
			PointBg p0 = pointsMax.get(i);
			PointBg p1 = pointsMin.get(i - 1);

			phase0 = 2 * PI + (0 - (2 * PI * p0.time / period)) % (2 * PI);
			phase1 = 2 * PI + (PI - (2 * PI * p1.time / period)) % (2 * PI);
			if (i < pointsMax.size()) {
				listPhases.add(phase0);
			}

			listPhases.add(phase1);
			phaseSum += phase0;
			phaseSum += phase1;

		}
		this.phaseMoyenne = phaseSum % (2 * PI);
		

	}

	private List<PointBg> getPoinsMaxFromList(List<PointBg> points2) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 1; i < (points2.size() - 1); i++) {
			PointBg p = points2.get(i);
			if ((p.x > points2.get(i - 1).x) && (p.x >= points2.get(i + 1).x)) {
				list.add(p);
			}
		}
		return list;
	}

	private List<PointBg> getPointMinFromList(List<PointBg> points2) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 1; i < (points2.size() - 1); i++) {
			PointBg p = points2.get(i);
			if ((p.x < points2.get(i - 1).x) && (p.x <= points2.get(i + 1).x)) {
				list.add(p);
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return "FrequenciesAnalyse [period=" + period + ", periodEcartType=" + periodEcartType + " amplitudeMoyenne :"
				+ amplitudeMoyenne + " amplitudeEcartType : " + amplitudeEcartType + "   phase Moyenne " + phaseMoyenne
				+ "  last Phase " + this.lastPhase + " nbDePoints " + nbDePoints + " lastPointHaut" + lastPointHaut
				+ " lastPointBas" + lastPointBas + ",\n frequenceAnalyseNext=" + frequenceAnalyseNext + "]";
	}

	public double getX(long t) {
		double x = 0;
		if (period != 0) {
			x = this.amplitudeMoyenne * Math.cos((2 * PI * t / this.period) + this.lastPhase)+moyenne;
		}
		double xNext = 0;
		if (this.frequenceAnalyseNext != null) {
			xNext = this.frequenceAnalyseNext.getX(t);
		}
		return x + xNext;
	}

}



