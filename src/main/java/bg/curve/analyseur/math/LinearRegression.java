package bg.curve.analyseur.math;


import java.util.List;

import bg.curve.PointBg;

public class LinearRegression {
	private final double intercept;
	private double slope;
	private final double r2;
	private final double svar0;
	private double svar1;

	

	public LinearRegression(List<PointBg> points) {
		int n = points.size();
		if (n == 0) {
			throw new IllegalArgumentException("no points ! points.size == 0");
		}

		// first pass
		double sumTime = 0.0;
		double sumy = 0.0;
		double sumTime2 = 0.0;
		for (PointBg p :points) {
			sumTime += p.time;
			sumTime2 += p.time * p.time;
			sumy +=p.x;
		}
		double timeBar = sumTime / n;
		double ybar = sumy / n;

		// second pass: compute summary statistics
		double ttimebar = 0.0;
		double yybar = 0.0;
		double xybar = 0.0;
		for (PointBg p :points) {
			ttimebar += (p.time - timeBar) * (p.time- timeBar);
			yybar += (p.x - ybar) * (p.x - ybar);
			xybar += (p.time - timeBar) * (p.x - ybar);
		}
		slope = xybar / ttimebar;
		intercept = ybar - slope * timeBar;

		// more statistical analysis
		double rss = 0.0; // residual sum of squares
		double ssr = 0.0; // regression sum of squares
		for (PointBg p :points) {
			double fit = slope * p.time + intercept;
			rss += (fit - p.x) * (fit -p.x);
			ssr += (fit - ybar) * (fit - ybar);
		}

		int degreesOfFreedom = n - 2;
		r2 = ssr / yybar;
		double svar = rss / degreesOfFreedom;
		svar1 = svar / ttimebar;
		svar0 = svar / n + timeBar * timeBar * svar1;
	}

	/**
	 * Returns the <em>x</em>-intercept &alpha; of the best of the best-fit line
	 * <em>x</em> = &alpha; + &beta; <em>time</em>.
	 *
	 * @return the <em>x</em>-intercept &alpha; of the best-fit line <em>x = &alpha;
	 *         + &beta; x</em>
	 */
	public double intercept() {
		return intercept;
	}

	/**
	 *
	 * @return the slope &beta; of the best-fit line <em>x</em> = &alpha; + &beta;
	 *         <em>time</em>
	 */
	public double slope() {
		return slope;
	}

	/**
	 * @return the coefficient of determination <em>R</em><sup>2</sup>, which is a
	 *         real number between 0 and 1
	 */
	public double R2() {
		return r2;
	}

	/**
	 * @return the standard error of the estimate for the intercept
	 */
	public double interceptStdErr() {
		return Math.sqrt(svar0);
	}

	/**	
	 * @return the standard error of the estimate for the slope
	 */
	public double slopeStdErr() {
		return Math.sqrt(svar1);
	}

	/**
	 * Returns the expected response {@codex} given the value of the predictor
	 * variable {@code time}.
	 *
	 * @param time 
	 * @return the expected response {@code x} given the value of the predictor
	 *         variable {@code time}
	 */
	public double predict(double time) {
		return slope * time + intercept;
	}

	
	public String toString() {
		String s =  String.format("%.2f n + %.2f", slope(), intercept());
		s+="  (R^2 = " + String.format("%.3f", R2()) + ")";
		return s;
	}

}
