package bg.futur.prevision;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public enum EnumDevises {

	ADA("ADA", "ada  cardano", 1l,"Cardano"), 
	BTC("BTC", "BTC", 2l,"Bitcoin"),
	ETH("ETH", "ETH",3l,"Ethereum"),
	XRP("XRP", "XRP",4l,"Ripple_(payment_protocol)"),
	LTC("LTC", "LTC",4l,"Litecoin"),
	;

	public final String shortName;
	public final String searchStringTwitter;
	public final String wikipediaPageUs;
	public final long id;  

	private EnumDevises(String shortName, String request, long id, String wikipediaPageUs) {
		this.shortName = shortName;
		this.searchStringTwitter = request;
		this.id = id;
		this.wikipediaPageUs = wikipediaPageUs;
	}

	public static EnumDevises getDevise(long idDevise) {
		for (EnumDevises devise : EnumDevises.values()) {
			if (devise.id == idDevise) {
				return devise;
			}
		}
		return null;
	}
	
	public static List<String> getDevisesShortNames(){
		List<String> list = new ArrayList<String>();
		for (EnumDevises devise : EnumDevises.values()) {
			list.add(devise.shortName);
		}
		return list;
	}

	public String getDevisePair() {
		
		return shortName+"USD";
	}

	public String  getWikipediaPageUs() {
		
		return URLEncoder.encode(this.wikipediaPageUs) ;

	}
	

}
