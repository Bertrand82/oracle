package bg.futur.prevision.wtachdog.bitfinex;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.wtachdog.bitfinex.BitfinexdataFetcher.INTERVALLE;


/**
 * https://api-pub.bitfinex.com/v2/tickers/hist?symbols=tBTCUSD
 * https://api-pub.bitfinex.com/v2/book/tBTCUSD/P0
 * candle :
 * https://api-pub.bitfinex.com/v2/stats1/pos.size:1m:tBTCUSD:long/hist
 * Available values: '1m', '5m', '15m', '30m', '1h', '3h', '6h', '12h', '1D', '7D', '14D', '1M'
 */
public class MainBitfinex {
	
	private static String URL_BITFINEX_STAT = "https://api-pub.bitfinex.com/v2/tickers/hist?symbols=tBTCUSD";

	public static DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
	public static void main(String[] args) throws Exception {
		System.err.println("URL_BITFINEX_STAT  "+ URL_BITFINEX_STAT);
		BitfinexdataFetcher fetcher  = new BitfinexdataFetcher(null);
		fetcher.fetchBitfinexDataProcess(EnumDevises.BTC, INTERVALLE.MN_5);
	}
	
	

	


	

}
