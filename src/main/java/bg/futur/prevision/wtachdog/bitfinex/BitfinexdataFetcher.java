package bg.futur.prevision.wtachdog.bitfinex;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IFetcher;
import bg.futur.prevision.architecture.UtilController;

@Component
public class BitfinexdataFetcher implements IFetcher{

	// https://api-pub.bitfinex.com/v2/tickers/hist?symbols=tBTCUSD

	// static String baseUrl = "https://api-pub.bitfinex.com/v2/";
	static String baseUrlBitfinex_ = "https://api-pub.bitfinex.com/v2/candles/trade:";
	static String baseUrlBitfinex_exemple = "https://api-pub.bitfinex.com/v2/candles/trade:1m:tBTCUSD/hist";

	enum PATH {
		TICKER("tickers/hist");

		String path;

		PATH(String p) {
			this.path = p;
		};
	}

	enum DEVISES_COUPLE______DEPRECATED {
		BTCUSD, XBTUSD
	};

	enum INTERVALLE {
		MN_1(1,"1m"), MN_5(5,"5m"), MN_15(15,"15m"), MN_30(30,"30m"), MN_60(60,"1h"), MN_240(240,"3h"), MN_1440_D_1(1440,"6h"), MN_10080_D_7(10080,"12h"), MN_21600_D_15(21600,"1d");

		private  int intervalle;
		private String pathQuery;

		private INTERVALLE(int intervalle2,String pathQuery) {
			this.intervalle = intervalle2;
			this.pathQuery = pathQuery;
		}

		public String toString() {
			return "" + intervalle;
		}

		public int getIntervalle() {
			return intervalle;
		}

		
		public String getPathQuery() {
			return pathQuery;
		}

	

		String getPath(EnumDevises devise) {
			return pathQuery + ":t" + devise.shortName + "USD" + "/hist";
		}
	};

	BitfinexdataRepository repository;

	public static void main(String[] args) throws Exception {

		try {
			// System.err.println(" Asset Info :::: " + getAssetInfo());
			// System.err.println(" Spreadinfo Info :::: " +
			// getRecentSpreads(DEVISES_COUPLE.BTCUSD));

			List<BitfinexData> list = fetchBitFinexDataProcess(EnumDevises.BTC, INTERVALLE.MN_30, new Date(0));
			System.err.println("pair : " + EnumDevises.BTC.getDevisePair());
			System.err.println(" OHCL  Info      :::: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + list.size());

			System.err.println("xxxxxxxxxxxx End xxxxxxxxxxxxx");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("end");

	}

	public BitfinexdataFetcher(BitfinexdataRepository repository) {
		this.repository = repository;
	}

	private static List<BitfinexData> fetchBitFinexDataProcess(EnumDevises devise, INTERVALLE intervalle, Date since) throws Exception {
		JsonNode node = getBitfinexData(devise, intervalle, since);
		List<BitfinexData> listData = new ArrayList<BitfinexData>();
		System.err.println("node :" + node);
		System.err.println("node  getNodeType :" + node.getNodeType());
		System.err.println("node isArray :" + node.isArray());
		for (final JsonNode objNode : node) {

			ObjectMapper mapper2 = new ObjectMapper();

			long time_ms = objNode.get(0).asLong();
			long v1_open = objNode.get(1).asLong();
			long v2_close = objNode.get(2).asLong();
			long v3_high = objNode.get(3).asLong();
			long v4_low = objNode.get(4).asLong();
			double v5_volume = objNode.get(5).asDouble();

			BitfinexData data = new BitfinexData("" + devise + "USD", intervalle.intervalle, time_ms, v1_open, v2_close, v3_high, v4_low, v5_volume);
			System.err.println("data    " + data + "    " + objNode);
			listData.add(data);
		}
		return listData;
	}

	/*
	 * https://api.kraken.com/0/public/OHLC?pair=XBTUSD
	 */
	private static JsonNode getBitfinexData(EnumDevises devise, INTERVALLE intervalle, Date since) throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		if (since != null) {
			list.add(new BasicNameValuePair("start", "" + since.getTime()));
		}
		JsonNode nodeResult = getRequest_get_asNodeJson_(intervalle.getPath(devise), list);
		return nodeResult;
	}

	/**
	 * 
	 * @param pair
	 * @return
	 * @throws Exception
	 */
	private static JsonNode getRecentSpreads(EnumDevises devise) throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("pair", "" + devise.getDevisePair()));
		JsonNode nodeResult = getRequest_get_asNodeJson_(PATH.TICKER.path, list);

		return nodeResult;
	}

	private static JsonNode getRequest_get_asNodeJson_(String path, List<NameValuePair> list) throws Exception {
		String resulthttp = getRequest_get(path, list);
		ObjectMapper mapper = new ObjectMapper();
		System.err.println("resulthttp " + resulthttp);
		JsonNode resultJsonNode = mapper.readTree(resulthttp);
		return resultJsonNode;
	}

	private static String getRequest_get(String path, List<NameValuePair> queryParameters) throws IOException, URISyntaxException {

		String responseStr = null;

		HttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

		String urlStr = baseUrlBitfinex_ + path;

		System.err.println("-------------------------  url : " + urlStr);
		System.err.println("-------------------------  url : " + baseUrlBitfinex_exemple);
		URIBuilder uriBuilder = new URIBuilder(urlStr);

		uriBuilder.addParameters(queryParameters);
		URI uri = uriBuilder.build();
		System.err.println("-------------------------  uri : " + uri);
		HttpGet httpGet = new HttpGet(uri);
		// httpGet.setHeader("Authorization", String.format("Bearer %s", bearerToken));
		// httpGet.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			responseStr = EntityUtils.toString(entity, "UTF-8");
		}
		return responseStr;
	}

	protected Date getLastDateEnd(EnumDevises devise, INTERVALLE interval) {
		String devisePairName = "" + devise.getDevisePair();
		Pageable pageable = UtilController.getPageable(1);;
		if (repository == null) {
			return null;
		}
		List<BitfinexData> list = repository.findByDevisePairAndInterval(devisePairName, interval.intervalle, pageable);
		System.err.println("getLastDateEnd ---------------------    devisePair " + devisePairName);
		System.err.println("getLastDateEnd ---------------------    Intervalle " + interval.intervalle);
		System.err.println("getLastDateEnd ---------------------    list " + list);
		if (list == null || list.isEmpty()) {
			return null;
		}
		BitfinexData tc = list.get(0);
		Date dateEnd = tc.getDate();

		System.out.println(" ");
		return dateEnd;
	}

	public List<BitfinexData> fetchBitfinexDataProcess(EnumDevises devise, INTERVALLE intervalle) throws Exception {
		Date since = getLastDateEnd(devise, intervalle);
		System.err.println("since :"+since+" |  devise:  "+devise);
		return fetchBitFinexDataProcess(devise, intervalle, since);
	}

	@Override
	public Date getLastDateEnd(EnumDevises devise, FREQUENCE frequence) {
		
		return getLastDateEnd(devise, getIntervalleFromFrequence(frequence));
	}

	private INTERVALLE getIntervalleFromFrequence(FREQUENCE frequence) {
		if (frequence == FREQUENCE.DAY) {
			return INTERVALLE.MN_60;
		}else if (frequence == FREQUENCE.HOUR) {
			return INTERVALLE.MN_5;
		}else {
		  return INTERVALLE.MN_5;
		}
	}

	@Override
	public String getSource() {
		
		return "Bitfinex";
	}
	
	

}
