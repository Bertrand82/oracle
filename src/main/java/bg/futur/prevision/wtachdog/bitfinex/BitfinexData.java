package bg.futur.prevision.wtachdog.bitfinex;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BitfinexData {

	public static DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Id
	@GeneratedValue
	private long id; // still set automatically
	private String devisePair;
	private int interval;
	private Date date;
	private double open;
	private double high;
	private double low;
	private double close;
	private double vwap;
	private double volume;
	private int count;

	public BitfinexData() {
		super();
	}

	@Deprecated
	public BitfinexData(String devisePair, int interval, long time, long v12, long v22, long v32, long v42, long v52) {
		this.devisePair = devisePair;
		this.interval = interval;
		this.date = new Date(time);
		/*
		 * this.open = Double.parseDouble(v12); this.high = Double.parseDouble(v22);
		 * this.low = Double.parseDouble(v32); this.close = Double.parseDouble(v42);
		 * this.vwap = Double.parseDouble(v52); this.volume = Double.parseDouble(x62);
		 * this.count = i2;
		 */
	}

	public BitfinexData(String devisepair, int intervalle, long time_ms, long v1_open, long v2_close, long v3_high, long v4_low, double v5_volume) {
		this.devisePair= devisepair;
		this.interval=intervalle;
		this.date = new Date(time_ms);
		this.open =v1_open;
		this.close = v2_close;
		this.high=v3_high;
		this.low=v4_low;
		this.volume = v5_volume;
		
	}

	@Override
	public String toString() {
		return "KrakenOHLdata [id=" + id + ", devisePair=" + devisePair + ", interval=" + interval + ", date=" +df1.format(date)  + ", open=" + open + ", high=" + high + ", low=" + low + ", close=" + close + ", vwap=" + vwap + ", volume=" + volume + ", count=" + count + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDevisePair() {
		return devisePair;
	}

	public void setDevisePair(String devisePair) {
		this.devisePair = devisePair;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getVwap() {
		return vwap;
	}

	public void setVwap(double vwap) {
		this.vwap = vwap;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
