package bg.futur.prevision.wtachdog.bitfinex;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;




@Repository
public interface BitfinexdataRepository extends CrudRepository<BitfinexData, Long>{

	
	List<BitfinexData> findByDevisePair(String lastName,Pageable pageable);
	List<BitfinexData> findByDevisePairAndInterval(String devise, int interval,Pageable pageable);
	List<BitfinexData> findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(String devise, int interval,Date since, Date to,Pageable pageable);


}
