package bg.futur.prevision.wtachdog.bitfinex;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.architecture.UtilController;
import bg.futur.prevision.board.IController;
import bg.futur.prevision.vega.PointTimeVega;

@RestController
@CrossOrigin
public class BitfinexDataController implements IController {

	private BitfinexdataRepository repository;

	public BitfinexDataController(BitfinexdataRepository repository) {
		this.repository = repository;
	}

	/**
	 * http://localhost/bitfinex/ohl/5/BTC
	 * 
	 * @param devise
	 * @param interval
	 * @param total
	 * @return
	 */
	@GetMapping("/bitfinex/ohl/{interval}/{devise}")
	List<BitfinexData> getBitfinexValues(@PathVariable String devise, @PathVariable int interval, @RequestParam(defaultValue = "100") int total, @RequestParam(defaultValue = "0") long since, @RequestParam(defaultValue = "0") long to) {
		if (to == 0) {
			to = System.currentTimeMillis();
		}
		if (since == 0) {
			since = UtilController.getSinceHourlyDefault();
		}
		Pageable pageable = UtilController.getPageable(total);
		String devisePair = devise + "USD";
		List<BitfinexData> list = repository.findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(devisePair, interval, new Date(since), new Date(to), pageable);
		System.err.println("------------- getBitfinexValues- devise : " + devise + " -----size :" + list.size() + "   interval :" + interval + "    total : " + total);

		return list;
	}

	// http://localhost//bitfinex/D/BTC
	@GetMapping("/bitfinex/D/{devise}")
	@CrossOrigin
	public List<PointTimeVega> getBitfinexValueForVegaDay(@PathVariable String devise, @RequestParam(defaultValue = "100") int total, @RequestParam(defaultValue = "0") long since, @RequestParam(defaultValue = "0") long to) {
		if (to == 0) {
			to = System.currentTimeMillis();
		}
		if (since == 0) {
			since = UtilController.getSinceDaillyDefault();
		}
		return getBitfinexValueForVega(devise, 60, new Date(since), new Date(to), total);
	}

	@GetMapping("/bitfinex/H/{devise}")
	@CrossOrigin
	public List<PointTimeVega> getBitfinexValueForVegaHour(@PathVariable String devise, @RequestParam(defaultValue = "100") int total, @RequestParam(defaultValue = "0") long since, @RequestParam(defaultValue = "0") long to) {
		if (to == 0) {
			to = System.currentTimeMillis();
		}
		if (since == 0) {
			since = UtilController.getSinceHourlyDefault();
		}
		int intervall = 5;
		return getBitfinexValueForVega(devise, intervall, new Date(since), new Date(to), total);

	}

	public List<PointTimeVega> getBitfinexValueForVega(String devise, int interval, Date since, Date to, int total) {

		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		String devisePair = devise + "USD";
		List<BitfinexData> listBitFinexData = repository.findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(devisePair, interval, since, to, pageable);
		for (BitfinexData tc : listBitFinexData) {
			PointTimeVega pt = new PointTimeVega(devise + " High", tc.getDate(), tc.getHigh());
			listPoints.add(pt);
		}
		System.err.println("------------- getBitfinexValueForVega - devisePair : " + devisePair + " -----size :" + listPoints.size() + "   interval :" + interval + "    total : " + total + "   since: " + since + "  to : " + to);
		return listPoints;
	}

	public List<PointTimeVega> getValuesForVega(String devise, int interval, Date since, Date to, int total) {

		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		String devisePair = devise + "USD";
		List<BitfinexData> listBitFinexData = repository.findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(devisePair, interval, since, to, pageable);
		for (BitfinexData tc : listBitFinexData) {
			PointTimeVega pt = new PointTimeVega(devise + " High", tc.getDate(), tc.getHigh());
			listPoints.add(pt);
		}
		System.err.println("------------- getBitfinexValueForVega - devisePair : " + devisePair + " -----size :" + listPoints.size() + "   interval :" + interval + "    total : " + total + "   since: " + since + "  to : " + to);
		return listPoints;
	}

	@Override
	public String getSourceName() {
		return "bitfinex";
	}
	@Override
	public String getSourceNameShort() {
		return "b";
	}

	@Override
	public List<PointTimeVega> getValuesForVega(String devise, INTERVALLE interval2, Date since, Date to, int total) {
		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		String devisePair = devise + "USD";
		int intervalle = 5;
		if (interval2 == INTERVALLE.DAY) {
			intervalle = 60;
		}
		List<BitfinexData> listBitFinexData = repository.findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(devisePair, intervalle, since, to, pageable);
		if (!listBitFinexData.isEmpty()) {
			double firstValue = listBitFinexData.get(listBitFinexData.size()-1).getHigh();
			System.err.println("------------- getBitfinexValueForVega -firstvalue : "+firstValue);
			for (BitfinexData tc2 : listBitFinexData) {
				System.err.println(devise+"--"+tc2.getHigh()+"   ;   "+firstValue+"   ;   "+tc2.getHigh()/firstValue);
						
				PointTimeVega pt = new PointTimeVega(this.getSourceNameShort() + " " + devise + " High", tc2.getDate(), (tc2.getHigh() / firstValue));
				listPoints.add(pt);
			}
		}
		System.err.println("------------- getBitfinexValueForVega - devisePair : " + devisePair + " -----size :" + listPoints.size() + "   interval :" + intervalle + "    total : " + total + "   since: " + since + "  to : " + to);
		return listPoints;
	}
}
