package bg.futur.prevision.wtachdog.bitfinex;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IWatchDog;
import bg.futur.prevision.architecture.IWatchDogHourly;
import bg.futur.prevision.wtachdog.twitter.TwitCountFetcher;

@Component
public class BitfinexWatchdogHourly implements IWatchDogHourly{

	@Autowired
	BitfinexdataRepository repository;
	
	@Autowired
	private BitfinexdataFetcher fetcher;
	
	
	@Override
	public void process() {
		for(EnumDevises devise : EnumDevises.values()) {
			processDevise(devise);
		}
	}
	
	private void processDevise(EnumDevises devise) {
		try {
			System.err.println("-----------------------------------     KrakenwatchdogHourly");
			 List<BitfinexData>  list = fetcher.fetchBitfinexDataProcess(devise, BitfinexdataFetcher.INTERVALLE.MN_5);
			 repository.saveAll(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
