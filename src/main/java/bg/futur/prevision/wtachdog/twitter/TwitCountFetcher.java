package bg.futur.prevision.wtachdog.twitter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IFetcher;

/**
 * Voir doc
 * :https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference/get-tweets-counts-recent
 * 
 * @author Bertrand
 *
 */

@Component
public class TwitCountFetcher extends TwitCountFetcherAbstract implements IFetcher{

	
	public TwitCountFetcher() {
		super();
	}

	public TwitCountFetcher(TwitterCountRepository repository) {
		super(repository);
	}

	
	public List<TwitterCount2> processDay(EnumDevises devise ) throws Exception {
		LocalDateTime  lastDateRegistered = getLastTwitterCountDateEnd(devise,Granulometrie.DAY);		
		System.err.println("Day  first twittercount lastDAte : " + lastDateRegistered);
		List<TwitterCount2> list = new ArrayList<TwitterCount2>();
		LocalDateTime dateTimeNow = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
		LocalDateTime dateTimeEnd = dateTimeNow.withHour(0).withMinute(0).withSecond(0).withNano(0);

		LocalDateTime dateTimeStart = dateTimeEnd.plusDays(-6);
		if(dateTimeStart.isBefore(lastDateRegistered.plusSeconds(+1))) {
			dateTimeStart = lastDateRegistered;
		}
		if (!dateTimeStart.isBefore(dateTimeEnd)) {
			System.err.println("date already recorded lastDateRegistered : "+lastDateRegistered+"   dateTimeEnd :"+dateTimeEnd);
			return list;
		}
		try {
			List<TwitterCount2> l = processSimple(devise, dateTimeStart, dateTimeEnd,Granulometrie.DAY);
			list.addAll(l);
		} catch (Exception e) {
			System.err.println("EXCEPTION " + devise + "  message: \n " + e.getMessage());
		}

		return list;
	}

	
	public List<TwitterCount2> processHour(EnumDevises devise ) throws Exception {
		LocalDateTime  lastDateRegistered = getLastTwitterCountDateEnd(devise,Granulometrie.HOUR);	
		System.err.println("Hour  first twittercount lastDAte : " + lastDateRegistered);
		LocalDateTime dateTimeNow = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC).plusMinutes(-1);
		LocalDateTime dateTimeEnd = dateTimeNow.withMinute(0).withSecond(0).withNano(0);
		LocalDateTime dateTimeStart = dateTimeEnd.plusHours(-24);
		if(dateTimeStart.isBefore(lastDateRegistered.plusSeconds(+1))) {
			dateTimeStart = lastDateRegistered;
		}
		List<TwitterCount2> list = new ArrayList<TwitterCount2>();
		if (!dateTimeStart.isBefore(dateTimeEnd)) {
			System.err.println("date already recorded lastDateRegistered : "+lastDateRegistered+"   dateTimeEnd :"+dateTimeEnd);
			return list;
		}
		try {
			List<TwitterCount2> lista = processSimple(devise, dateTimeStart, dateTimeEnd,Granulometrie.HOUR);
			list.addAll(lista);
		} catch (Exception e) {
			System.err.println("EXCEPTION " + devise + "  message: \n " + e.getMessage());
		}
		return list;
	}

	@Override
	public Date getLastDateEnd(EnumDevises devise, FREQUENCE frequence) {
		Granulometrie granulometrie  = Granulometrie.DAY;
		if(frequence == FREQUENCE.HOUR) {
			granulometrie = Granulometrie.HOUR;
		}
		LocalDateTime localDate =  getLastTwitterCountDateEnd(devise, granulometrie);
		Date date =Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant());
		return date;
	}

	
	@Override
	public String getSource() {		
		return "Twitter";
	}
	

}
