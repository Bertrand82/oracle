package bg.futur.prevision.wtachdog.twitter;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterCountRepository extends CrudRepository<TwitterCount2, Long>{

	List<TwitterCount2> findByDevise(String lastName,Pageable pageable);
	List<TwitterCount2> findByDeviseAndGranulometrie(String devise, String granulometrie,Pageable pageable);
}
