package bg.futur.prevision.wtachdog.twitter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.architecture.UtilController;
import bg.futur.prevision.board.IController;
import bg.futur.prevision.board.IController.INTERVALLE;
import bg.futur.prevision.vega.PointTimeVega;
import bg.futur.prevision.wtachdog.twitter.TwitCountFetcherAbstract.Granulometrie;

@RestController
@CrossOrigin
public class TwitterCountController implements IController {

	TwitterCountRepository repository;

	TwitterCountController(TwitterCountRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/twitter/data/{granulometrie}/{devise}")
	List<TwitterCount2> getTwitterCount(@PathVariable String devise, @PathVariable String granulometrie, @RequestParam(defaultValue = "10") int total) {

		Pageable pageable =  UtilController.getPageable(total);
		return repository.findByDeviseAndGranulometrie(devise, granulometrie, pageable);
	}

	@GetMapping("/twitter/{granulometrie}/{devise}")
	@CrossOrigin
	List<PointTimeVega> getTwitterTimeCount(@PathVariable String devise, @PathVariable String granulometrie, @RequestParam(defaultValue = "100") int total) {
		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable = UtilController.getPageable(total);
		List<TwitterCount2> listTwitterCount = repository.findByDeviseAndGranulometrie(devise, granulometrie, pageable);
		for (TwitterCount2 tc : listTwitterCount) {
			PointTimeVega pt = new PointTimeVega(devise, tc.getDate(), tc.getTweet_countAsInt());
			listPoints.add(pt);
		}
		System.err.println("------------- getTwitterTimeCount - devise : " + devise + " -----size :" + listPoints.size() + "   granulometrie :" + granulometrie + "    total : " + total);
		return listPoints;
	}

	@Override
	public String getSourceName() {
		return "Twitter";
	}

	@Override
	public List<PointTimeVega> getValuesForVega(String devise, INTERVALLE interval, Date since, Date to, int total) {
		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		Granulometrie granulometrie = Granulometrie.HOUR;
		if (interval == INTERVALLE.DAY) {
			granulometrie = Granulometrie.DAY;
		}
		List<TwitterCount2> listTwitterCount = repository.findByDeviseAndGranulometrie(devise, granulometrie.shortName, pageable);
		if (!listTwitterCount.isEmpty()) {
			double firstValue = listTwitterCount.get(0).getTweet_countAsInt();
			for (TwitterCount2 twiterCount : listTwitterCount) {
				PointTimeVega pt = new PointTimeVega(getSourceNameShort()+" "+ devise, twiterCount.getDate(), twiterCount.getTweet_countAsInt()/firstValue);
				listPoints.add(pt);
			}
		}
		System.err.println("------------- getTwitterTimeCount - devise : " + devise + " -----size :" + listPoints.size() + "   granulometrie :" + granulometrie + "    total : " + total);
		return listPoints;
	}

	@Override
	public String getSourceNameShort() {
		return "T";
	}

}
