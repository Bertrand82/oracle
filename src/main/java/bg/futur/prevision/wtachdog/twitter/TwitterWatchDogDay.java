package bg.futur.prevision.wtachdog.twitter;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IWatchDogDayly;
import bg.futur.prevision.architecture.IWatchDogHourly;

@Component
public class TwitterWatchDogDay implements IWatchDogDayly{ 	
	
	  @Autowired
	  private TwitterCountRepository repository;
	  
	  @Autowired
	  private TwitCountFetcher twitCountFactoryDay;
	  
	@Override
	public void process() {
		
		System.err.println("----------------- WatchDogTwitterDay ----------------------------------------------------- start "+new Date());
		for(EnumDevises devise : EnumDevises.values()) {
			System.err.println("----------------- twitterWatchDog   devise: "+devise.shortName+"  start ");
			try {
				List<TwitterCount2> listTwitterCount = twitCountFactoryDay.processDay(devise);
				for(TwitterCount2 twitterCount : listTwitterCount) {					
					repository.save(twitterCount);
					System.err.println("Saved in database "+devise.shortName+"    "+twitterCount);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	}
}
