package bg.futur.prevision.wtachdog.twitter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import bg.futur.prevision.wtachdog.wikipedia.WikipediaData;
import lombok.Data;

@Entity
@Data
public class TwitterCount2 {
	
	public static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	@Id
	@GeneratedValue
	long id; // still set automatically
	private Date date;
	private Date start;
	private String tweet_count;
	private String devise;
	private String granulometrie ;

	
	

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	} 
	@JsonGetter("end")
	public Date getDate() {
		return date;
	}
	@JsonSetter("end")
	public void setDate(Date d) {
		this.date = d;
	} 
	
	public void setStart(String startStr) {
		try {

			this.start = df.parse(startStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getTweet_count() {
		return tweet_count;
	}

	public int getTweet_countAsInt() {
		try {
			return Integer.parseInt(tweet_count);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	public void setTweet_count(String tweet_count) {
		this.tweet_count = tweet_count;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDevise() {
		return devise;
	}

	public void setDevise(String devise) {
		this.devise = devise;
	}

	public String getGranulometrie() {
		return granulometrie;
	}

	public void setGranulometrie(String granulometrie) {
		this.granulometrie = granulometrie;
	}

	@Override
	public String toString() {
		return "TwitterCount [id=" + id + ", date=" + date + ", start=" + start + ", tweet_count=" + tweet_count + ", devise=" + devise + ", granulometrie=" + granulometrie + "]";
	}

	

}
