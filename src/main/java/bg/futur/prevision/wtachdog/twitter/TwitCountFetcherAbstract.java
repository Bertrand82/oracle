package bg.futur.prevision.wtachdog.twitter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.UtilController;
import bg.futur.prevision.wtachdog.twitter.TwitCountFetcherAbstract.Granulometrie;

public abstract class TwitCountFetcherAbstract {

	public enum Granulometrie {
		DAY("day", "D"), HOUR("hour", "H"), MINUTE("minute", "M");

		String granulometrieName;
		String shortName;

		Granulometrie(String v, String shortNAme) {
			this.granulometrieName = v;
			this.shortName = shortNAme;
		}

		public String toString() {
			return granulometrieName;
		}
	}

	static protected DateFormat dateFormatTwitter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	static protected DateTimeFormatter dtformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

	public static Properties pOAuth = new Properties();
	public static final String KEY_oauth_consumerKey = "oauth.consumerKey";
	public static final String KEY_oauth_consumerSecret = "oauth.consumerSecret";
	public static final String KEY_oauth_accessToken = "oauth.accessToken";
	public static final String KEY_oauth_accessTokenSecret = "oauth.accessTokenSecret";
	public static final String KEY_bearer_Token = "bearer_Token";

	public static final LocalDateTime DATE0 = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC).withYear(0).withHour(0).withMinute(0).withSecond(0);

	static {
		try {
			initOAuthProperties();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] a) {
		System.out.println(Granulometrie.DAY);
		System.out.println(Granulometrie.DAY.toString());
		System.out.println(Granulometrie.DAY.granulometrieName);
		System.out.println(Granulometrie.DAY.shortName);
	}

	public static void initOAuthProperties() throws Exception {
		File file = new File("twitter4j.properties");
		System.out.println("File : " + file.getAbsolutePath() + " exists " + file.exists());
		if (file.exists()) {
			FileReader reader = new FileReader(file);
			pOAuth.load(reader);
			System.out.println("Bearer token " + pOAuth.getProperty(KEY_bearer_Token));
		}else {
			System.err.println("NO File :"+file.getAbsolutePath());
		}
	}

	@Autowired
	protected TwitterCountRepository repository;

	public TwitCountFetcherAbstract(TwitterCountRepository repository) {
		this.repository = repository;
	}

	public TwitCountFetcherAbstract() {

	}

	public static LocalDateTime convertToLocalDateTimeViaMilisecond(Date dateToConvert) {
		if (dateToConvert == null) {
			return DATE0;
		}
		return Instant.ofEpochMilli(dateToConvert.getTime()).atZone(ZoneOffset.UTC).toLocalDateTime();
	}

	protected List<TwitterCount2> processSimple(EnumDevises devise, LocalDateTime dateTimeStart, LocalDateTime dateTimeEnd, Granulometrie granulometrie) throws Exception {
		List<TwitterCount2> listTwitterCount = new ArrayList<>();
		String result = getTweetCounts(devise.searchStringTwitter, pOAuth.getProperty(KEY_bearer_Token), dateTimeStart, dateTimeEnd, granulometrie);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println("result : " + result);
		JsonNode resultJsonNode = mapper.readTree(result);
		System.out.println("resultJsonNode " + resultJsonNode);
		JsonNode dataNode = resultJsonNode.get("data");
		System.out.println("data isArray :" + dataNode.isArray());
		if (dataNode.isArray()) {
			for (final JsonNode objNode : dataNode) {
				System.out.println(objNode);
				ObjectMapper mapper2 = new ObjectMapper();
				TwitterCount2 twitterCount = mapper2.treeToValue(objNode, TwitterCount2.class);
				twitterCount.setDevise(devise.shortName);
				twitterCount.setGranulometrie(granulometrie.shortName);
				System.out.println(twitterCount);
				listTwitterCount.add(twitterCount);
			}
		}
		return listTwitterCount;
	}

	private String getTweetCounts(String searchString, String bearerToken, LocalDateTime dateTimeStart, LocalDateTime dateTimeEnd, Granulometrie granulometrie) throws IOException, URISyntaxException {
		String searchResponse = null;

		HttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

		URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/2/tweets/counts/recent");
		ArrayList<NameValuePair> queryParameters;
		queryParameters = new ArrayList<>();
		queryParameters.add(new BasicNameValuePair("query", searchString));
		queryParameters.add(new BasicNameValuePair("granularity", granulometrie.granulometrieName));

		String startTime = dtformatter.format(dateTimeStart);
		String endTime = dtformatter.format(dateTimeEnd);

		queryParameters.add(new BasicNameValuePair("start_time", startTime));
		queryParameters.add(new BasicNameValuePair("end_time", endTime));
		uriBuilder.addParameters(queryParameters);
		System.err.println(" start Time " + startTime);
		System.err.println(" end  Time " + endTime);
		HttpGet httpGet = new HttpGet(uriBuilder.build());
		httpGet.setHeader("Authorization", String.format("Bearer %s", bearerToken));
		httpGet.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			searchResponse = EntityUtils.toString(entity, "UTF-8");
		}
		return searchResponse;
	}

	protected LocalDateTime getLastTwitterCountDateEnd(EnumDevises devise, Granulometrie granulometrie) {
		String deviseShort = devise.shortName;
		Pageable pageable = UtilController.getPageable(1);
		List<TwitterCount2> list = repository.findByDeviseAndGranulometrie(deviseShort, granulometrie.shortName, pageable);
		System.err.println("---------------------    deviseShort " + deviseShort);
		System.err.println("---------------------    list " + list);
		if (list == null || list.isEmpty()) {
			return DATE0;
		}
		TwitterCount2 tc = list.get(0);
		Date dateEnd = tc.getDate();
		LocalDateTime ldt = convertToLocalDateTimeViaMilisecond(dateEnd);
		return ldt;
	}

}
