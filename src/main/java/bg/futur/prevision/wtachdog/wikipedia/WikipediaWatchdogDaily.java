package bg.futur.prevision.wtachdog.wikipedia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IWatchDog;
import bg.futur.prevision.architecture.IWatchDogDayly;
import bg.futur.prevision.architecture.IWatchDogHourly;
import bg.futur.prevision.wtachdog.kraken.KrakenOHLdata;
import bg.futur.prevision.wtachdog.kraken.KrakenOHLdataFetcher;
import bg.futur.prevision.wtachdog.twitter.TwitCountFetcher;

@Component
public class WikipediaWatchdogDaily implements IWatchDogDayly {

	@Autowired
	WikipediaDataRepository repository;

	@Autowired
	private WikipediaFetcher fetcher;

	@Override
	public void process() {
			System.err.println("-----------------------------------    WikipediawatchdogDaily");
			for(EnumDevises devise : EnumDevises.values()) {
				processDevise(devise);
			}
		
	}

	private void processDevise(EnumDevises devise) {
		System.err.println("-----------------------------------     KrakenwatchdogDaily");
		try {
			List<WikipediaData> list = fetcher.fetchWikipediaProcess(devise);
			repository.saveAll(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
