package bg.futur.prevision.wtachdog.wikipedia;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import bg.futur.prevision.wtachdog.twitter.TwitterCount2;


@Repository
public interface WikipediaDataRepository extends CrudRepository<WikipediaData, Long>{

	
	List<WikipediaData> findByDevise(String devise,Pageable pageable);


}
