package bg.futur.prevision.wtachdog.wikipedia;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;


@Entity
@Data
public class WikipediaData {

	
	private int  views  ;
	private Date date ;
	private String devise;
	@Id
	@GeneratedValue
	private long id; // still set automatically

	public WikipediaData() {
		super();
	}

	public WikipediaData(int views , Date date, String devise) {
		super();
		this.views = views;
		this.date = date;
		this.devise = devise;
	}
	
}
