package bg.futur.prevision.wtachdog.wikipedia;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;


/**
 * https://pageviews.toolforge.org/?project=fr.wikipedia.org&platform=all-access&agent=user&redirects=0&range=latest-90&pages=Bitcoin|Ripple_(protocole_de_paiement)#
 * https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/Bitcoin/daily/20210904/20211001
 * 
 * @author w1
 *
 */
public class MainWikipedia {
	
	private static String URL_WIKIPEDIA_STAT = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/Bitcoin/daily/";

	public static DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
	public static void main(String[] args) throws Exception {
		System.err.println(WikipediaFetcher.getUrlsWikipedia());
		JsonNode node = getOHLCData();
		System.err.println(" xxxxxxxxxxxxxxxxxxxxxxxxxxxxx    node "+node);
		System.err.println(" xxxxxxxxxxxxxxxxxxxxxxxxxxxxx    node node.isArray "+node.isArray());
		WikipediaFetcher fetcher = new WikipediaFetcher(null);
		 List<WikipediaData>  list  =fetcher.fetchWikipediaProcess(EnumDevises.BTC);
		 System.err.println("list wikipediadata size : "+list.size());
		 System.err.println("list wikipediadata : "+list);
	}
	
	
	
	private static JsonNode getOHLCData() throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.roll(Calendar.DAY_OF_YEAR, -30);
		Date dateStart =calendar.getTime();
		System.out.println("dateStart ::: "+df1.format(dateStart));
		JsonNode nodeResult = getRequest_get(dateStart, list);
		return nodeResult;
	}



	


	private static JsonNode getRequest_get(Date dateStart, List<NameValuePair> list) throws Exception {
		String urlStr = URL_WIKIPEDIA_STAT+df1.format(dateStart)+"/"+df1.format(new Date());
		System.out.println("urlStr "+urlStr);
		String resulthttp = getRequest_get(urlStr, list);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode resultJsonNode = mapper.readTree(resulthttp);
		JsonNode nodeResult = resultJsonNode.get("items");

		return nodeResult;
	}



	private static String getRequest_get(String urlStr, List<NameValuePair> queryParameters) throws Exception {
		String responseStr = null;

		HttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();
		
		System.err.println("-------------------------  url : " + urlStr);
		URIBuilder uriBuilder = new URIBuilder(urlStr);

		uriBuilder.addParameters(queryParameters);

		HttpGet httpGet = new HttpGet(uriBuilder.build());
		// httpGet.setHeader("Authorization", String.format("Bearer %s", bearerToken));
		// httpGet.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			responseStr = EntityUtils.toString(entity, "UTF-8");
		}
		System.err.println("-------------------------  responseStr: " + responseStr);
		return responseStr;
	}

}
