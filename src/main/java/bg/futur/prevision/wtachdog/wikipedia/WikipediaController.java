package bg.futur.prevision.wtachdog.wikipedia;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.architecture.UtilController;
import bg.futur.prevision.board.IController;
import bg.futur.prevision.vega.PointTimeVega;
import bg.futur.prevision.wtachdog.twitter.TwitCountFetcherAbstract.Granulometrie;

@RestController
@CrossOrigin
public class WikipediaController implements IController{

	private WikipediaDataRepository repository;
	
	WikipediaController(WikipediaDataRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/wikipedia/data/D/{devise}")
	@CrossOrigin
	List<WikipediaData> getWikipediaCount(@PathVariable String devise, @RequestParam(defaultValue = "10") int total){
		
		Pageable pageable =  UtilController.getPageable(total);
		return repository.findByDevise(devise,pageable);
	}
	
	@GetMapping("/wikipedia/D/{devise}")
	@CrossOrigin
	List<PointTimeVega> getTwitterTimeCount(@PathVariable String devise,  @RequestParam(defaultValue = "100") int total){
		List<PointTimeVega>  listPoints  = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		List<WikipediaData>  listWikipediaCount  = repository.findByDevise(devise,pageable);
		for(WikipediaData tc:listWikipediaCount) {
			PointTimeVega pt = new PointTimeVega(devise, tc.getDate(), tc.getViews());
			listPoints.add(pt);
		}
		System.err.println("------------- getTwitterTimeCount - devise : "+devise+" -----size :"+listPoints.size()+"    total : "+total);
		return listPoints;
	}

	@Override
	public String getSourceName() {
		return "wikipedia";
	}

	@Override
	public List<PointTimeVega> getValuesForVega(String devise, INTERVALLE interval, Date since, Date to, int total) {
		List<PointTimeVega>  listPoints  = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		List<WikipediaData>  listWikipediaCount  = repository.findByDevise(devise,pageable);
		for(WikipediaData tc:listWikipediaCount) {
			PointTimeVega pt = new PointTimeVega(getSourceNameShort()+" "  +devise, tc.getDate(), tc.getViews());
			listPoints.add(pt);
		}
		System.err.println("------------- getTwitterTimeCount - devise : "+devise+" -----size :"+listPoints.size()+"    total : "+total);
		return listPoints;
	}

	@Override
	public String getSourceNameShort() {
		return "w";
	}
	
	
}
