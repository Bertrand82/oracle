package bg.futur.prevision.wtachdog.wikipedia;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IFetcher;
import bg.futur.prevision.architecture.UtilController;



/**
 * https://pageviews.toolforge.org/?project=fr.wikipedia.org&platform=all-access&agent=user&redirects=0&range=latest-90&pages=Bitcoin|Ripple_(protocole_de_paiement)#
 * https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/Bitcoin/daily/20210904/20211001
 * 
 * @author w1
 *
 */
@Component
public class WikipediaFetcher implements IFetcher{
	
	public static DateFormat df1 = new SimpleDateFormat("yyyyMMddHH");
	public static DateFormat df1_ = new SimpleDateFormat("yyyyMMdd");
	
	private static String URL_WIKIPEDIA_STAT = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/${pagewikipedia}/daily/";

	
	public static void main(String[] args) throws Exception {
		WikipediaFetcher fetcher = new WikipediaFetcher(null);
		 List<WikipediaData> list =  fetcher.fetchWikipediaProcess(EnumDevises.BTC);
	}
	
	WikipediaDataRepository repository;
	
	
	
	public WikipediaFetcher(WikipediaDataRepository repository) {
		super();
		this.repository = repository;
	}






	private  JsonNode getWikipediaCData(EnumDevises enumDevise) throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.roll(Calendar.DAY_OF_YEAR, -30);
		Date dateStart =calendar.getTime();
		System.out.println("dateStart ::: "+df1.format(dateStart));
		JsonNode nodeResult = getJsonNodeWikipedia_get(dateStart, list,enumDevise);
		return nodeResult;
	}



	public static String getUrlsWikipedia() {
		String s ="";
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.roll(Calendar.DAY_OF_YEAR, -30);
		Date dateStart =calendar.getTime();
		for(EnumDevises devise : EnumDevises.values()) {
			String urlStr = URL_WIKIPEDIA_STAT+df1.format(dateStart)+"/"+df1.format(new Date());
			urlStr = urlStr.replace("${pagewikipedia}", devise.getWikipediaPageUs());
			s+=urlStr+"\n";
		}
		return s;
	}


	private  JsonNode getJsonNodeWikipedia_get(Date dateStart, List<NameValuePair> list,EnumDevises devise) throws Exception {
		
		Date dateLast  = this.getLastDateEnd(devise);
		if (dateLast!= null) {
			if (dateLast.after(dateStart)){
				dateStart = dateLast;
			}
		}
		
		String urlStr = URL_WIKIPEDIA_STAT+df1.format(dateStart)+"/"+df1.format(new Date());
		urlStr = urlStr.replace("${pagewikipedia}", devise.getWikipediaPageUs());
		
		System.out.println("urlStr "+urlStr);
		String resulthttp = getStringResponse_get(urlStr, list);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode resultJsonNode = mapper.readTree(resulthttp);
		JsonNode nodeResult = resultJsonNode.get("items");

		return nodeResult;
	}



	private static String getStringResponse_get(String urlStr, List<NameValuePair> queryParameters) throws Exception {
		String responseStr = null;

		HttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();
		
		System.err.println("-------------------------  url : " + urlStr);
		URIBuilder uriBuilder = new URIBuilder(urlStr);

		uriBuilder.addParameters(queryParameters);

		HttpGet httpGet = new HttpGet(uriBuilder.build());
		// httpGet.setHeader("Authorization", String.format("Bearer %s", bearerToken));
		// httpGet.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			responseStr = EntityUtils.toString(entity, "UTF-8");
		}
		System.err.println("-------------------------  responseStr: " + responseStr);
		return responseStr;
	}


/**
 * "project": "en.wikipedia",
		"article": "Bitcoin",
		"granularity": "daily",
		"timestamp": "2021090900",
		"access": "all-access",
		"agent": "all-agents",
		"views": 22085
 * @param devise
 * @return
 * @throws Exception
 */
	public List<WikipediaData> fetchWikipediaProcess(EnumDevises devise) throws Exception{
		List<WikipediaData> listData = new ArrayList<WikipediaData>();
		JsonNode node  = getWikipediaCData(devise);          
		for (final JsonNode objNode : node) {
			int views  = objNode.get("views").asInt();
			String timeStamp = objNode.get("timestamp").asText();
			Date date = df1.parse(timeStamp);
			WikipediaData wd = new WikipediaData(views, date, devise.shortName);
			listData.add(wd);
		}
		return listData;
	}
	
	protected Date getLastDateEnd(EnumDevises devise) {
		if (repository == null) {
			return null;
		}
		String deviseStr =""+ devise.shortName;
		Pageable pageable = UtilController.getPageable(1);
		List<WikipediaData> list = repository.findByDevise(deviseStr,  pageable);
		System.err.println("---------------------    devise " + deviseStr);
		System.err.println("---------------------    list " + list);
		if (list == null || list.isEmpty()) {
			return  null;
		}
		WikipediaData wd = list.get(0);
		Date dateEnd = wd.getDate();
		return dateEnd;
	}






	@Override
	public Date getLastDateEnd(EnumDevises devise, FREQUENCE frequence) {
		if (frequence == FREQUENCE.DAY) {
			return getLastDateEnd(devise);
		}
		return new Date(0);
	}
	
	@Override
	public String getSource() {		
		return "Wikipedia";
	}

}
