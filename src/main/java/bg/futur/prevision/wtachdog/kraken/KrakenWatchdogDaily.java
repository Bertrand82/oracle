package bg.futur.prevision.wtachdog.kraken;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IWatchDog;
import bg.futur.prevision.architecture.IWatchDogDayly;
import bg.futur.prevision.architecture.IWatchDogHourly;
import bg.futur.prevision.wtachdog.twitter.TwitCountFetcher;

@Component
public class KrakenWatchdogDaily implements IWatchDogDayly {

	@Autowired
	KrakenOHLdataRepository repository;

	@Autowired
	private KrakenOHLdataFetcher fetcher;

	@Override
	public void process() {
		for(EnumDevises devise : EnumDevises.values()) {
			processDevise(devise);
		}
	}
	
	private void processDevise(EnumDevises devise) {
		try {
			System.err.println("-----------------------------------     KrakenwatchdogDaily");
			List<KrakenOHLdata> list = fetcher.fetchOHLCDataProcess(devise, KrakenOHLdataFetcher.INTERVALLE.MN_60);
			repository.saveAll(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
