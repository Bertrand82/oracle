package bg.futur.prevision.wtachdog.kraken;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import bg.futur.prevision.wtachdog.twitter.TwitterCount2;


@Repository
public interface KrakenOHLdataRepository extends CrudRepository<KrakenOHLdata, Long>{

	
	List<KrakenOHLdata> findByDevisePair(String lastName,Pageable pageable);
	List<KrakenOHLdata> findByDevisePairAndInterval(String devise, int interval,Pageable pageable);
	List<KrakenOHLdata> findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(String devise, int interval,Date since, Date to,Pageable pageable);


}
