package bg.futur.prevision.wtachdog.kraken;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IFetcher;
import bg.futur.prevision.architecture.UtilController;

@Component
public class KrakenOHLdataFetcher implements IFetcher{

	static String baseUrl = "https://api.kraken.com/0/";
	private static final Logger log = LoggerFactory.getLogger(KrakenOHLdataFetcher.class);

	enum PATH {
		ASSET("public/Assets"), SPREAD("public/Spread"), OHLC("public/OHLC");

		String path;

		PATH(String p) {
			this.path = p;
		};
	}

	enum DEVISES_COUPLE______DEPRECATED {
		BTCUSD, XBTUSD
	};

	enum INTERVALLE {
		MN_1(1), MN_5(5), MN_15(15), MN_30(30), MN_60(60), MN_240(240), MN_1440_D_1(1440), MN_10080_D_7(10080), MN_21600_D_15(21600);

		int intervalle;

		private INTERVALLE(int intervalle2) {
			this.intervalle = intervalle2;
		}

		public String toString() {
			return "" + intervalle;
		}
	};
	
	KrakenOHLdataRepository repository;

	public static void main(String[] args) throws Exception {

		try {
			// System.err.println(" Asset Info :::: " + getAssetInfo());
			// System.err.println(" Spreadinfo Info :::: " +
			// getRecentSpreads(DEVISES_COUPLE.BTCUSD));

			List<KrakenOHLdata> list = getOHLCDataProcess(EnumDevises.BTC, INTERVALLE.MN_30,new Date(0));
			System.err.println("pair : " + EnumDevises.BTC.getDevisePair());
			System.err.println(" OHCL  Info      :::: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + list.size());

			System.err.println("xxxxxxxxxxxx End xxxxxxxxxxxxx");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("end");

	}
	
	public KrakenOHLdataFetcher(	KrakenOHLdataRepository repository) {
		this.repository =repository;
	}


	private  static List<KrakenOHLdata> getOHLCDataProcess(EnumDevises devise, INTERVALLE intervalle, Date since) throws Exception {
		JsonNode node = getOHLCData(devise, intervalle,since);
		List<KrakenOHLdata> listData = new ArrayList<KrakenOHLdata>();
		System.err.println("node  getNodeType :" + node.getNodeType());

		System.err.println("node isArray :" + node.isArray());
		JsonNode node0 = node.elements().next();
		System.err.println("node0  getNodeType :" + node0.getNodeType());
		System.err.println("node0 isArray :" + node0.isArray());

		System.err.println("node0 isArray :" + node0.isArray());
		for (final JsonNode objNode : node0) {

			ObjectMapper mapper2 = new ObjectMapper();

			long time = objNode.get(0).asLong() * 1000;
			String v1 = "" + objNode.get(1).asText();
			String v2 = "" + objNode.get(2).asText();
			String v3 = "" + objNode.get(3).asText();
			String v4 = "" + objNode.get(4).asText();
			String v5 = "" + objNode.get(5).asText();
			String v6 = "" + objNode.get(6).asText();
			int i = objNode.get(7).asInt();
			KrakenOHLdata ohlData = new KrakenOHLdata("" + devise.getDevisePair(), intervalle.intervalle, time, v1, v2, v3, v4, v5, v6, i);
			System.err.println("ohlData    " + ohlData + "    " + objNode);
			listData.add(ohlData);
		}
		return listData;
	}

	/*
	 * https://api.kraken.com/0/public/OHLC?pair=XBTUSD
	 */
	private static JsonNode getOHLCData(EnumDevises devise, INTERVALLE intervalle,Date since) throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("pair", "" + devise.getDevisePair()));
		list.add(new BasicNameValuePair("interval", "" + intervalle));
		list.add(new BasicNameValuePair("since", "" + since.getTime()/1000));
		JsonNode nodeResult = getRequest_get_asNodeJson(PATH.OHLC.path, list);
		return nodeResult;
	}

	/**
	 * 
	 * @param pair
	 * @return
	 * @throws Exception
	 */
	private static JsonNode getRecentSpreads(EnumDevises devise) throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("pair", "" + devise.getDevisePair()));
		JsonNode nodeResult = getRequest_get_asNodeJson(PATH.SPREAD.path, list);

		return nodeResult;
	}

	private static JsonNode getRequest_get_asNodeJson(String path, List<NameValuePair> list) throws Exception {
		String resulthttp = getRequest_get(path, list);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode resultJsonNode = mapper.readTree(resulthttp);
		JsonNode nodeResult = resultJsonNode.get("result");

		return nodeResult;
	}

	private static String getRequest_get(String path, List<NameValuePair> queryParameters) throws IOException, URISyntaxException {

		String responseStr = null;

		HttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();
		String urlStr = baseUrl + path;
		System.err.println("-------------------------  url : " + urlStr);
		URIBuilder uriBuilder = new URIBuilder(urlStr);

		uriBuilder.addParameters(queryParameters);

		HttpGet httpGet = new HttpGet(uriBuilder.build());
		// httpGet.setHeader("Authorization", String.format("Bearer %s", bearerToken));
		// httpGet.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			responseStr = EntityUtils.toString(entity, "UTF-8");
		}
		return responseStr;
	}
	


	protected Date getLastDateEnd2(EnumDevises devisePair, INTERVALLE interval) {
		String devisePairName =""+ devisePair.getDevisePair();
		Pageable pageable =  UtilController.getPageable(1);
		List<KrakenOHLdata> list = repository.findByDevisePairAndInterval(devisePairName, interval.intervalle, pageable);
		System.err.println("getLastDateEnd kraken ---------------------    devisePair " + devisePairName);
		System.err.println("getLastDateEnd kraken---------------------    list " + list);
		if (list == null || list.isEmpty()) {
			return  new Date(0);
		}
		KrakenOHLdata tc = list.get(0);
		Date dateEnd = tc.getDate();		
		return dateEnd;
	}

	public List<KrakenOHLdata> fetchOHLCDataProcess(EnumDevises devisepair, INTERVALLE mn) throws Exception{
		Date since = getLastDateEnd2(devisepair, mn);
		
		List<KrakenOHLdata> list = getOHLCDataProcess(devisepair, mn, since);
		log.info("fetchOHLCDataProcess devisepair :"+devisepair+" intervalle :"+mn+" since : "+ since+" returned  list.size "+list.size());
		return list;
	}

	@Override
	public Date getLastDateEnd(EnumDevises devise, FREQUENCE frequence) {
		
		Date date = getLastDateEnd2(devise, getIntervalleFromFrequence(frequence));
		return date;
	}

	private INTERVALLE getIntervalleFromFrequence(FREQUENCE frequence) {
		if (frequence == FREQUENCE.DAY) {
			return INTERVALLE.MN_60;
		}else if (frequence == FREQUENCE.HOUR) {
			return INTERVALLE.MN_5;
		}else {
		  return INTERVALLE.MN_5;
		}
	}
	@Override
	public String getSource() {
		
		return "Kraken";
	}
}
