package bg.futur.prevision.wtachdog.kraken;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.architecture.UtilController;
import bg.futur.prevision.board.IController;
import bg.futur.prevision.board.IController.INTERVALLE;
import bg.futur.prevision.vega.PointTimeVega;

@RestController
@CrossOrigin
public class KrakenOHLdataController implements IController {

	Logger logger = LoggerFactory.getLogger(KrakenOHLdataController.class);
	KrakenOHLdataRepository repository;

	KrakenOHLdataController(KrakenOHLdataRepository repository) {
		this.repository = repository;
		logger.warn("KrakenOHLdataController Constructor");
	}

	/**
	 * http://localhost/kraken/ohl/5/BTC
	 * 
	 * @param devise
	 * @param interval
	 * @param total
	 * @return
	 */
	@GetMapping("/kraken/ohl/{interval}/{devise}")
	List<KrakenOHLdata> getKrakenValues(@PathVariable String devise, @PathVariable int interval, @RequestParam(defaultValue = "100") int total) {

		Pageable pageable =  UtilController.getPageable(total);
		String devisePair = devise + "USD";
		List<KrakenOHLdata> list = repository.findByDevisePairAndInterval(devisePair, interval, pageable);
		System.err.println("------------- getKrakenValues- devise : " + devise + " -----size :" + list.size() + "   interval :" + interval + "    total : " + total);

		return list;
	}

	@GetMapping("/kraken/D/{devise}")
	@CrossOrigin
	public List<PointTimeVega> getKrakenValueForVegaDay(@PathVariable String devise, @RequestParam(defaultValue = "100") int total, @RequestParam(defaultValue = "0") long since, @RequestParam(defaultValue = "0") long to) {
		int interval = 60;
		if (to == 0) {
			to = System.currentTimeMillis();
		}
		if (since == 0) {
			since = UtilController.getSinceHourlyDefault();
		}
		return getKrakenValueForVega(devise, interval, new Date(since), new Date(to), total);

	}

	/**
	 * 
	 * @param devise
	 * @param total
	 * @return
	 */
	@GetMapping("/kraken/H/{devise}")
	@CrossOrigin
	public List<PointTimeVega> getKrakenValueForVegaHour(@PathVariable String devise, @RequestParam(defaultValue = "100") int total, @RequestParam(defaultValue = "0") long since, @RequestParam(defaultValue = "0") long to) {
		if (to == 0) {
			to = System.currentTimeMillis();
		}
		if (since == 0) {
			since = UtilController.getSinceHourlyDefault();
		}
		int intervall = 5;
		return getKrakenValueForVega(devise, intervall, new Date(since), new Date(to), total);
	}

	/**
	 * 
	 * @param devise
	 * @param interval
	 * @param total
	 * @return
	 */

	private List<PointTimeVega> getKrakenValueForVega(String devise, int interval, int total) {

		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		String devisePair = devise + "USD";
		List<KrakenOHLdata> listTwitterCount = repository.findByDevisePairAndInterval(devisePair, interval, pageable);
		for (KrakenOHLdata tc : listTwitterCount) {
			PointTimeVega pt = new PointTimeVega(devise + " High", tc.getDate(), tc.getHigh());
			listPoints.add(pt);
		}
		System.err.println("------------- getKrakenValueForVega - devise : " + devise + " -----size :" + listPoints.size() + "   interval :" + interval + "    total : " + total);
		return listPoints;
	}

	/**
	 * 
	 * @param devise
	 * @param interval
	 * @param total
	 * @return
	 */

	private List<PointTimeVega> getKrakenValueForVega(String devise, int interval, Date since, Date to, int total) {

		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable =  UtilController.getPageable(total);
		String devisePair = devise + "USD";
		List<KrakenOHLdata> listTwitterCount = repository.findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(devisePair, interval, since, to, pageable);
		for (KrakenOHLdata tc : listTwitterCount) {
			PointTimeVega pt = new PointTimeVega(devise + " High", tc.getDate(), tc.getHigh());
			listPoints.add(pt);
		}
		this.logger.warn("------------- getKrakenValueForVega - devise : " + devise + " -----size :" + listPoints.size() + "   interval :" + interval + "    total : " + total + " since:  " + since + "  to:  " + to + "    sizeReturned " + listPoints.size());
		System.err.println("------------- getKrakenValueForVega2 - devise : " + devise + " -----size :" + listPoints.size() + "   interval :" + interval + "    total : " + total + " since:  " + since + "  to:  " + to);
		return listPoints;
	}

	@Override
	public String getSourceName() {
		return "kraken";
	}

	@Override
	public List<PointTimeVega> getValuesForVega(String devise, INTERVALLE interval, Date since, Date to, int total) {
		List<PointTimeVega> listPoints = new ArrayList<PointTimeVega>();
		Pageable pageable = UtilController.getPageable(total);
		String devisePair = devise + "USD";
		int intervalle = 5;
		if (interval == INTERVALLE.DAY) {
			intervalle = 60;
		}
		List<KrakenOHLdata> listKraken = repository.findByDevisePairAndIntervalAndDateGreaterThanAndDateLessThan(devisePair, intervalle, since, to, pageable);
		if (!listKraken.isEmpty()) {
			double firstValue = listKraken.get(0).getHigh();
			for (KrakenOHLdata tc : listKraken) {
				PointTimeVega pt = new PointTimeVega(getSourceNameShort()+" "+  devise + " High", tc.getDate(), tc.getHigh() / firstValue);
				listPoints.add(pt);
			}
		}
		this.logger.warn("------------- getKrakenValueForVega - devise : " + devise + " -----size :" + listPoints.size() + "   interval :" + interval +"  "+intervalle+ "    total : " + total + " since:  " + since + "  to:  " + to + "    sizeReturned " + listPoints.size());
		System.err.println("------------- getKrakenValueForVega2 - devise : " + devise + " -----size :" + listPoints.size() + "   interval :" + interval +"  "+intervalle+ "    total : " + total + " since:  " + since + "  to:  " + to);
		return listPoints;
	}

	@Override
	public String getSourceNameShort() {
		return "k";
	}

}
