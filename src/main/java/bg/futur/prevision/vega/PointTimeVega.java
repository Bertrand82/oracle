package bg.futur.prevision.vega;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PointTimeVega {

	
	
	public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
	public static DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final private String label ;
	final private String time;
	final private double y;
	
	public PointTimeVega(String label, Date time,double y){
		this.time = df1.format(time);
		this.label=label;
		//this.x = time.getTime();
		this.y =y;
	}
	
	 public String toString() {
		 return label+","+this.time+","+((int)this.y);
	 }
	public static void main(String[] s) {
		System.out.println("start");
		System.out.println("start "+df.format(new Date()));
	}

	public String getTime() {
		return time;
	}

	public double getY() {
		return y;
	}

	public String getLabel() {
		return label;
	}


	

	

	
}
