package bg.futur.prevision;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import bg.futur.prevision.architecture.IWatchDog;
import bg.futur.prevision.architecture.IWatchDogDayly;
import bg.futur.prevision.architecture.IWatchDogHourly;

@Component
public class ScheduledTasks {
	
	
	@Autowired
	private List<IWatchDogDayly> listWatchDogDaily;
	@Autowired
	private List<IWatchDogHourly> listWatchDogHourly;

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	// @Scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of  week] [Year]")
	// @Scheduled(cron = "0 0 12 * * ?") // Fires at 12 PM every day:
	// @Scheduled(cron = "0 15 10 * * ? 2005") //Fires at 10:15 AM every day in the  year 2005:
	//@Scheduled(cron = "0 15 10 * * ?") // Fires at 12 PM every day:
	//@Scheduled(cron = "0/20 * * * * ?")  // Fires every 20 seconds:
	//@Scheduled(cron = "0 * * * * ?")  // Fires every mn.00 :
	@Scheduled(cron = "0 0 * * * ?")  // Fires every h Omn.00 :
	public void startProcessHourly() {
		System.err.println("StartProcess hourly  The time is now " + dateFormat.format(new Date())+"  "+listWatchDogHourly);
		for(IWatchDog watchdog : listWatchDogHourly) {
			watchdog.process();
		}
	}
	
	@Scheduled(cron = "2 15 10 * * ?") // Fires at 12 PM every day:
	public void startProcessDayly() {
		System.err.println("StartProcess dayly The time is now " + dateFormat.format(new Date())+"  "+listWatchDogDaily);
		for(IWatchDog watchdog : listWatchDogDaily) {
			watchdog.process();
		}
	}
}