package bg.futur.prevision;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling

@EnableAutoConfiguration

//@EntityScan(basePackages = { "bg.spring.generated.pojo","bg.persistence.tool.common"}) 
@EnableJpaAuditing
public class MainOracle {

	public static void main(String[] args) {
		
		SpringApplication springApplication = new SpringApplicationBuilder(MainOracle.class)
				.properties(getProperties())
				.build();
		
		ConfigurableApplicationContext context = springApplication.run(args);
		//SpringApplication.run(MainPrevisionApplication.class, args);
	}
	
	
	private static String getProperties() {
		return "spring.config.location:" + "classpath:config-app.yml,"+ "classpath:config-database.yml";
	}

}
