package bg.futur.prevision.board;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.architecture.IFetcher;
import bg.futur.prevision.architecture.IFetcher.FREQUENCE;
import bg.futur.prevision.vega.PointTimeVega;

@RestController
@CrossOrigin
public class SupervisionController {

	
	@Autowired
	private List<IFetcher> listFetchers;
	/**
	 * http://localhost/supervision/lastdates
	 * @return
	 */
	@GetMapping("/supervision/lastdates")
	@CrossOrigin
	public List<Object> getLastDates( ) {
		List<Object> listDates = new ArrayList<Object>();
		for(IFetcher fetcher : listFetchers) {
			
			for(EnumDevises devise : EnumDevises.values()) {
				List<Object> listDatesFetcher = new ArrayList<Object>();
				listDatesFetcher.add(fetcher.getSource());
				listDatesFetcher.add(devise);
				Date dateD  =  fetcher.getLastDateEnd(devise, FREQUENCE.DAY);
				Date dateH  =  fetcher.getLastDateEnd(devise, FREQUENCE.HOUR);
				listDatesFetcher.add(dateH);
				listDatesFetcher.add(dateD);
				listDates.add(listDatesFetcher);
				System.err.println("getLastDates ource "+fetcher.getSource()+" devise "+devise+"  dateD :"+dateD+"    dateH :"+dateH);
			}
			
		}
		return listDates;
	}
	
}
