package bg.futur.prevision.board;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.EnumDevises;
import bg.futur.prevision.wtachdog.bitfinex.BitfinexWatchdogDaily;
import bg.futur.prevision.wtachdog.bitfinex.BitfinexWatchdogHourly;
import bg.futur.prevision.wtachdog.kraken.KrakenWatchdogDaily;
import bg.futur.prevision.wtachdog.kraken.KrakenWatchdogHourly;
import bg.futur.prevision.wtachdog.twitter.TwitterCount2;
import bg.futur.prevision.wtachdog.twitter.TwitterWatchDogDay;
import bg.futur.prevision.wtachdog.twitter.TwitterWatchDogHour;
import bg.futur.prevision.wtachdog.wikipedia.WikipediaWatchdogDaily;

@RestController
@CrossOrigin
public class WatchDogController {

	@Autowired
	private TwitterWatchDogDay twitterWatchDogDay;
	@Autowired
	private TwitterWatchDogHour twitterWatchDogHour;
	@Autowired
	private KrakenWatchdogHourly krakenWatchdogHourly;
	@Autowired
	private KrakenWatchdogDaily krakenWatchdogDaily;
	@Autowired
	private WikipediaWatchdogDaily wikipediaWatchdogDaily;
	@Autowired
	private BitfinexWatchdogHourly bitfinexchdogHourly;
	@Autowired
	private BitfinexWatchdogDaily bitfinexnWatchdogDaily;

	@GetMapping("/board/twitter/daily")
	@CrossOrigin
	public String  getTwitterDaily() {
		System.err.println("getTwitterDaily ");
		twitterWatchDogDay.process();
		return "twitterWatchDogDaily process done";
	}

	@GetMapping("/board/twitter/hourly")
	@CrossOrigin
	public String  getTwitterHourly() {
		System.err.println("getTwitterHourly ");
		twitterWatchDogHour.process();
		return "twitterWatchDogHour process done";
	}

	@GetMapping("/board/kraken/hourly")
	@CrossOrigin
	public String  getKrakenHourly() {
		System.err.println("getKrakenHourly ");
		krakenWatchdogHourly.process();
		return  "krakenWatchdogHourly done";
	}

	@GetMapping("/board/kraken/daily")
	@CrossOrigin
	public String getKrakenDaily() {
		System.err.println("getTwitterDaily ");
		krakenWatchdogDaily.process();
		return "Kraken daily started";
	}

	@GetMapping("/board/wikipedia/daily")
	@CrossOrigin
	public String getWikipediaDaily() {
		System.err.println("getWikipediaDaily ");
		wikipediaWatchdogDaily.process();
		return "wikipedia daily started";
	}

	@GetMapping("/devises")
	@CrossOrigin
	public List<String> getDevises() {
		System.err.println("getDevises ");
		return EnumDevises.getDevisesShortNames();
	}

	@GetMapping("/board/bitfinex/hourly")
	@CrossOrigin
	public String  getBitfinexHourly() {
		System.err.println("bitfinexchdogHourly starting ");
		this.bitfinexchdogHourly.process();
		return "bitfinexchdogHourly done "+new Date();
	}

	@GetMapping("/board/bitfinex/daily")
	@CrossOrigin
	public String getBitfinexDaily() {
		System.err.println("bitfinexnWatchdogDaily starting ");
		this.bitfinexnWatchdogDaily.process();
		return "bitfinexnWatchdogDaily daily done";
	}
	
	@GetMapping("/board/start/{source}/{granulometrie}")
	public String  startTwitterCount(@PathVariable String source,@PathVariable String granulometrie){
		
		boolean isAllIntervalles = granulometrie.equalsIgnoreCase("All");
		boolean isDaily = granulometrie.equalsIgnoreCase("D") || isAllIntervalles;
		boolean isHourly = granulometrie.equalsIgnoreCase("H") || isAllIntervalles;
		
		boolean isBitFinex = source.equalsIgnoreCase("bitfinex");
		boolean isKraken= source.equalsIgnoreCase("kraken");
		boolean isTwitter= source.equalsIgnoreCase("twitter");
		boolean isWikipedia= source.equalsIgnoreCase("wikipedia");
		boolean isAllSources = source.equalsIgnoreCase("all");
		
		String r ="";
		if (isBitFinex || isAllSources) {
			if (isDaily) {
				r += this.getBitfinexDaily()+" \n";
			}
			if (isHourly) {
				r += this.getBitfinexHourly()+" \n";
			}
		}
		if (isKraken || isAllSources) {
			if (isDaily) {
				r += this.getKrakenDaily()+" \n";
			}
			if (isHourly) {
				r += this.getKrakenHourly()+" \n";
			}
		}
		if (isTwitter || isAllSources) {
			if (isDaily) {
				r += this.getTwitterDaily()+" \n";
			}
			if (isHourly) {
				r += this.getTwitterHourly()+" \n";
			}
		}
		if (isWikipedia || isAllSources) {
			if (isDaily) {
				r += this.getWikipediaDaily()+" \n";
			}
			if (isHourly) {
				r += "No Wikipedia hourly"+" \n";
			}
		}	
		return " source : "+source+" granulometrie : "+granulometrie+"  result : "+r;
	}

}
