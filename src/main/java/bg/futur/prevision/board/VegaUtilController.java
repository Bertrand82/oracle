package bg.futur.prevision.board;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.vega.PointTimeVega;



@RestController
@CrossOrigin
public class VegaUtilController {
	public static final long MINUTE = 60l * 1000l;
	public static final long HOUR = 60l * MINUTE;
	public static final long DAY = 24l * HOUR;

	private List<PointTimeVega> getSinusoidePointTimeTest(int min, int max, double phase, String label) {
		List<PointTimeVega> list = new ArrayList<PointTimeVega>();
		long now = System.currentTimeMillis() - 20 * DAY;

		for (int i = min; i < max; i++) {
			Date date = new Date(now + i * MINUTE);
			Double y = 20 * Math.sin((2 * Math.PI * i / 100) + phase);
			if ((i > max / 2) && (i < 2 * max / 3)) {

			} else {
				PointTimeVega pt = new PointTimeVega("" + label, date, y);
				list.add(pt);
			}
		}
		return list;
	}
	
	private List<PointTimeVega> getSinusoidePointTimeTest2(int min, int max, double phase, String label1, String label2) {
		List<PointTimeVega> list = new ArrayList<PointTimeVega>();
		long now = System.currentTimeMillis() - 20 * DAY;

		for (int i = min; i < max; i++) {
			Date date = new Date(now + i * MINUTE);
			Double y = 20 * Math.sin((2 * Math.PI * i / 100) + phase);
			if ((i > max / 2) && (i < 2 * max / 3)) {

			} else {
				PointTimeVega pt = new PointTimeVega("" + label1, date, y);
				list.add(pt);
			}
		}
		for (int i = min; i < max; i++) {
			Date date = new Date(now + i * MINUTE);
			Double y = 20 * Math.sin((2 * Math.PI * i / 100) + phase +Math.PI);
			if ((i > max / 2) && (i < 2 * max / 3)) {

			} else {
				PointTimeVega pt = new PointTimeVega("" + label2, date, y);
				list.add(pt);
			}
		}
		return list;
	}


	@GetMapping("/testvega1")
	@CrossOrigin
	public List<PointTimeVega> getTest1() {
		System.err.println("Test Vega 1");
		List<PointTimeVega> list = getSinusoidePointTimeTest(1, 200, 0, "sinus");
		return list;

	}
	
	@GetMapping("/testvega2")
	@CrossOrigin
	public List<PointTimeVega> getTest2() {
		System.err.println("Test Vega 2 ");
		List<PointTimeVega> list = getSinusoidePointTimeTest2(1, 200, 0, "sinus","cos");
		return list;

	}

	

}
