package bg.futur.prevision.board;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bg.futur.prevision.architecture.UtilController;
import bg.futur.prevision.board.IController.INTERVALLE;
import bg.futur.prevision.vega.PointTimeVega;

@RestController
@CrossOrigin
public class MultiSourcesController {

	@Autowired
	private List<IController> listIControllers;

	/**
	 * http://localhost/multisources/D?sources=kraken&sources=bitfinex&devises=BTC&devises=ADA
	 * http://localhost/multisources/D?sources=kraken&devises=BTC&devises=ADA
	 * @param devise
	 * @param total
	 * @param since
	 * @param to
	 * @return
	 */
	@GetMapping("/multisources/D")
	@CrossOrigin
	public List<PointTimeVega> getValueForVegaDay(@RequestParam String[] sources, @RequestParam String[] devises, @RequestParam(defaultValue = "100") int total, @RequestParam(defaultValue = "0") long since, @RequestParam(defaultValue = "0") long to) {
		if (to == 0) {
			to = System.currentTimeMillis();
		}
		if (since == 0) {
			since = UtilController.getSinceDaillyDefault();
		}
		System.err.println("devises "+Arrays.toString(devises));
		System.err.println("sources "+Arrays.toString(sources));
		INTERVALLE intervalle = INTERVALLE.DAY;
		List<PointTimeVega> list = new ArrayList<PointTimeVega>();
		for (IController controllerData : this.listIControllers) {
			if (isSourceSelected(sources, controllerData)) {

				for (String devise : devises) {
					List<PointTimeVega> list0 = controllerData.getValuesForVega(devise, intervalle, new Date(since), new Date(to), total);
					list.addAll(list0);
				}
			}
		}

		return list;
	}

	private boolean isSourceSelected(String[] sources, IController controllerData) {
		for (String source : sources) {
			if (source.equalsIgnoreCase(controllerData.getSourceName())) {
				return true;
			}
		}
		return false;
	}
}
