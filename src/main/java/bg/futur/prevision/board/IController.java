package bg.futur.prevision.board;

import java.util.Date;
import java.util.List;

import bg.futur.prevision.vega.PointTimeVega;

public interface IController {
	
	enum INTERVALLE {DAY, HOUR};

	public String getSourceName();
	public String getSourceNameShort();
	
	public List<PointTimeVega> getValuesForVega( String devise,INTERVALLE interval, Date since , Date to, int total);
		
}
