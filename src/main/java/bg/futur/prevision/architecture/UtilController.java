package bg.futur.prevision.architecture;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class UtilController {
	public static final long HOUR_ms= 60*60*1000;
	public static final long DAY_ms = 24* HOUR_ms;
	
	public static  long getSinceHourlyDefault() {
		long now = System.currentTimeMillis();
		return now - DAY_ms;
	}
	
	
	public static long getSinceDaillyDefault() {
		long now = System.currentTimeMillis();
		return now - 20 * DAY_ms;
	}
	
	public static Pageable getPageable(int total) {
		Pageable pageable = PageRequest.of(0, total, Sort.by("date").descending());
		return pageable;
	}
}
