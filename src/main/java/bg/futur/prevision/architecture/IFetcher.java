package bg.futur.prevision.architecture;

import java.util.Date;

import bg.futur.prevision.EnumDevises;

public interface IFetcher {
	
	enum FREQUENCE {
		DAY, HOUR
	}
	public Date getLastDateEnd(EnumDevises devise, FREQUENCE frequence) ;
	
	public String getSource();

}
