package bg.futur.prevision.watchdog.bidon;

import org.springframework.stereotype.Component;

import bg.futur.prevision.architecture.IWatchDog;
import bg.futur.prevision.architecture.IWatchDogHourly;

@Component
public class WatchDogTest  implements IWatchDogHourly{

	@Override
	public void process() {
		System.err.println("WatchDogTest do nothing");
		
	}

}
