package bg.frequence.analyseur.math;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import bg.curve.analyseur.math.FrequenciesAnalyse;

public class TestFrequencies {

	@Test
	public void test() {
		long period0 = 10l;
		FrequenciesAnalyse fa = new FrequenciesAnalyse(GeneratorTest.generateSinusoid(period0));
		System.out.println("period0   " +period0);
		System.out.println("FrequenciesAnalyse    " +fa);
		Assert.assertEquals(period0, fa.period);
	}
	
	@Test
	public void test2() {
		long period1 = 10l;
		long period2 = 100l;
		FrequenciesAnalyse fa = new FrequenciesAnalyse(GeneratorTest.generate2(period1,period2));
		System.out.println("period1   " +period1);
		System.out.println("period2   " +period2);
		System.out.println("FrequenciesAnalyse    " +fa);
		Assert.assertEquals(period1, fa.period);
		Assert.assertEquals(period2, fa.frequenceAnalyseNext.period);
	}
	
	@Test
	public void test3() {
		long period1 = 10l;
		long period2 = 20l;
		long period3 = 100l;
		FrequenciesAnalyse fa = new FrequenciesAnalyse(GeneratorTest.generate3(period1,period2,period3));
		System.out.println("period1   " +period1);
		System.out.println("period2   " +period2);
		System.out.println("period3   " +period3);
		System.out.println("FrequenciesAnalyse    " +fa);
		Assert.assertEquals(period1, fa.period);
		Assert.assertEquals(period2, fa.frequenceAnalyseNext.period);
	}

}


