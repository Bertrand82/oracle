package bg.frequence.analyseur.math;

import java.util.ArrayList;
import java.util.List;

import bg.curve.PointBg;


public class GeneratorTest {

	public static List<PointBg> generateSinusoid(long period) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 0; i < 10000; i++) {
			long t = i * 1l;
			double x = Math.cos((2 * Math.PI * t) / period);
			PointBg p = new PointBg(t,x);
			list.add(p);
		}
		return list;
	}
	
	public static List<PointBg> generateDroite(double a, double b) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 0; i < 10000; i++) {
			long t = i * 1l;
			double x = a * t +b;
			PointBg p = new PointBg(t,x);
			list.add(p);
		}
		return list;
	}


	public static List<PointBg> generateArray(long period) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 0; i < 10000; i++) {
			long t = i * 1l;
			double x = Math.cos((2 * Math.PI * t) / period);
			PointBg p = new PointBg(x, t);
			list.add(p);
		}
		return list;
	}

	public static List<PointBg> generate2(long period1, long period2) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 0; i < 10000; i++) {
			long t = i * 1l;
			double x1 = 2.0 * Math.cos((2 * Math.PI * t) / period1);
			double x2 = Math.cos((2 * Math.PI * t) / period2);
			PointBg p = new PointBg(x1 + x2, t);
			list.add(p);
		}
		return list;
	}

	public static List<PointBg> generate3(long... periods) {
		List<PointBg> list = new ArrayList<PointBg>();
		for (int i = 0; i < 10000; i++) {
			long t = i * 1l;
			double x = 0;
			for (int j = 0; j < periods.length; j++) {
				long p = periods[j];
				double x1 = 2.0 * Math.cos((2 * Math.PI * t) / p);
				x += x1;
			}
			PointBg p = new PointBg(x, t);
			list.add(p);
		}
		return list;
	}
}



