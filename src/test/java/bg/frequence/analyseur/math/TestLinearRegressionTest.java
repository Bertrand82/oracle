package bg.frequence.analyseur.math;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import bg.curve.analyseur.math.FrequenciesAnalyse;
import bg.curve.analyseur.math.LinearRegression;

public class TestLinearRegressionTest {

	
	@Test
	public void test1() {
		System.err.println("start Test linearRegression");
		long period0 = 10l;
		LinearRegression lr = new LinearRegression(GeneratorTest.generateSinusoid(period0));
		System.out.println("period0   " +period0);
		System.out.println("Linear Regression    " +lr);
		Assert.assertEquals(0.0, lr.slope(),0.00001);
		Assert.assertEquals(0.0, lr.intercept(),0.001);
	}
	
	@Test
	public void test2() {
		System.err.println("start Test linearRegression");
		double a = 12.0;
		double b =24.0;
		LinearRegression lr = new LinearRegression(GeneratorTest.generateDroite(a, b));
		System.out.println("a    " +a+"    "+lr.slope());
		System.out.println("b    " +b+"    "+lr.intercept());
		System.out.println("Linear Regression    " +lr);
		Assert.assertEquals(a, lr.slope(),0.00001);
		Assert.assertEquals(b, lr.intercept(),0.001);
	}
	
	public static void main(String[] a) {
		TestLinearRegressionTest rlrt  = new TestLinearRegressionTest();
		rlrt.test1();
		rlrt.test2();
	}
}
